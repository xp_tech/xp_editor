import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {PagesBrowserComponent} from './pages-browser.component';
import {ActionsSubject, Store} from '@ngrx/store';
import {getActive, State as ArticlesState} from '../reducers/articles.reducer';
import {State as SettingsState} from '../../admin/reducers/settings.reducer';
import {LoadSettings} from '../../admin/actions/settings.actions';
import {takeUntil} from 'rxjs/operators';
import {ArticleModel} from '../models/articles.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import {getError} from '../reducers/chunks.reducer';
import {ArticlesActionTypes} from '../actions/articles.actions';

@Component({
  selector: 'app-articles',
  templateUrl: './page-tools.html',
  styles: []
})
export class ArticlesComponent implements OnInit, OnDestroy {

  selectedIndex = 1;
  data: ArticleModel;
  entity: 'pages' | 'articles' = 'articles';
  componentDestroyed$: Subject<boolean> = new Subject();

  @ViewChild('browser', { static: true }) browser: PagesBrowserComponent;

  constructor(private actionsSubj: ActionsSubject,
              private snackBar: MatSnackBar,
              private articlesStore: Store<ArticlesState>,
              private settingsStore: Store<SettingsState>) { }

  ngOnInit() {
    this.settingsStore.dispatch(new LoadSettings());

    this.articlesStore.select(getActive)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => this.data = data);

    this.actionsSubj
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data.type === ArticlesActionTypes.GetArticle && this.selectedIndex === 1) {
          this.selectedIndex = 0;
        }
      });

    this.articlesStore.select(getError)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data) this.snackBar.open(data, undefined, {duration: 3000});
      });
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  selectedTabChange() {
    if (this.selectedIndex === 1) this.browser.loadPages();
  }

}
