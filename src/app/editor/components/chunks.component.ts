import {Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {ActionsSubject, Store} from '@ngrx/store';
import {ChunksBrowserComponent} from './chunks-browser.component';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ChunksActionTypes} from '../actions/chunks.actions';
import { MatSnackBar } from '@angular/material/snack-bar';
import {getError, State as ChunksState} from '../reducers/chunks.reducer';


@Component({
  selector: 'app-chunks',
  template: `
    <section fxLayout="row" fxLayoutGap="2em">
      <div fxFlex="80">
        <mat-tab-group [(selectedIndex)]="selectedIndex" (selectedTabChange)="selectedTabChange()">
          <mat-tab label="Editor">
            <app-chunks-editor></app-chunks-editor>
          </mat-tab>
          <mat-tab label="Browser">
            <app-chunks-browser #browser></app-chunks-browser>
          </mat-tab>
        </mat-tab-group>
      </div>

      <div fxFlex="20">
        <h1>Tags</h1>
        <app-tags-editor entity="chunks" mode="{{(selectedIndex === 0) ? 'editor' : 'browser'}}"></app-tags-editor>
      </div>
    </section>



  `,
  styles: []
})
export class ChunksComponent implements OnInit, OnDestroy {

  selectedIndex = 1;
  componentDestroyed$: Subject<boolean> = new Subject();

  @ViewChild('browser', { static: true }) browser: ChunksBrowserComponent;

  constructor(private snackBar: MatSnackBar,
              private chunksStore: Store<ChunksState>,
              private actionsSubj: ActionsSubject) { }

  ngOnInit() {
    this.actionsSubj
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data.type === ChunksActionTypes.GetChunk && this.selectedIndex === 1) {
          this.selectedIndex = 0;
        }
      });

    this.chunksStore.select(getError)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data) this.snackBar.open(data, undefined, {duration: 3000});
      });

  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  selectedTabChange() {
    if (this.selectedIndex === 1) this.browser.loadChunks();
  }

}
