import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {getList, getPath, State as FilesState} from '../reducers/files.reducer';
import {ActionsSubject, Store} from '@ngrx/store';
import {takeUntil} from 'rxjs/operators';
import {CreateDir, DeleteDir, DeleteFile, FilesActionTypes, LoadFiles, UploadFile} from '../actions/files.actions';
import {FileModel} from '../models/files.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import {host} from '../../utils/router.utils';
import {getError} from "../reducers/chunks.reducer";
import {WarningSnackbarComponent} from "../../shared/components/warning-snackbar.component";

@Component({
  selector: 'app-files',
  template: `
    <section fxLayout="row" fxLayoutGap="2em">
      <div fxFlex="80">
        <div fxLayout="row" fxLayoutGap="1em" fxLayoutAlign="center center">
          <div fxFlex="40">

            <form>
              <input type="file" class="d-hidden" #fileinput (change)="onFileSelected($event)">
              <button mat-raised-button color="primary" class="mx-1" (click)="fileinput.click()">
                <mat-icon aria-label="Upload file" class="mr-1">backup</mat-icon> Upload file
              </button>
            </form>
          </div>
          <div fxFlex="60" class="text-right">
            <mat-form-field>
              <input matInput #fName placeholder="Folder name" maxlength="32" [formControl]="folderName">
              <mat-error *ngIf="folderName.hasError('pattern')">
                Not a valid name (letters and _ only)
              </mat-error>
              <mat-error *ngIf="folderName.hasError('required')">
                Folder name is <strong>required</strong>
              </mat-error>
              <mat-hint align="end">{{fName.value.length}} / 64</mat-hint>
            </mat-form-field>

            <button mat-mini-fab class="mx-1" [disabled]="!(folderName.valid)"
                    (click)="createFolder()">
              <mat-icon aria-label="Create folder">create_new_folder</mat-icon>
            </button>
          </div>
        </div>

        <table mat-table [dataSource]="dataSource" matSort class="w-100">

          <ng-container matColumnDef="folder">
            <th mat-header-cell class="w-50-px" *matHeaderCellDef></th>
            <td mat-cell class="w-50-px" *matCellDef="let element">
              <mat-icon *ngIf="element.type === 'directory'" aria-label="Folder">folder</mat-icon>
            </td>
          </ng-container>

          <ng-container matColumnDef="name">
            <th mat-header-cell class="cell-link" *matHeaderCellDef (click)="goUp()">
              <strong>{{(path !== '') ? '&#8592; ' : ''}}{{(path === '') ? '/' : path}} </strong></th>
            <td mat-cell class="cell-link"  *matCellDef="let element" (click)="onSelected(element)"> {{element.name}} </td>
          </ng-container>

          <ng-container matColumnDef="mime">
            <th mat-header-cell class="w-100-px text-center" *matHeaderCellDef> mime </th>
            <td mat-cell class="w-100-px text-center" *matCellDef="let element"> {{(element.mime) ? element.mime.split('/').reverse()[0] : undefined}} </td>
          </ng-container>

          <ng-container matColumnDef="size">
            <th mat-header-cell class="w-200-px text-center" *matHeaderCellDef> size </th>
            <td mat-cell class="w-200-px text-center" *matCellDef="let element">
              {{setFileSize(element.size)}}
            </td>
          </ng-container>

          <ng-container matColumnDef="options">
            <th mat-header-cell class="w-100-px text-center"  *matHeaderCellDef> options </th>
            <td mat-cell class="w-100-px text-center" *matCellDef="let element">
              <button mat-icon-button color="warn" (click)="delete(element.type, element.name)">
                <mat-icon aria-label="Delete element">delete</mat-icon>
              </button>
            </td>
          </ng-container>

          <tr mat-header-row *matHeaderRowDef="displayedColumns; sticky: true"></tr>
          <tr mat-row *matRowDef="let row; columns: displayedColumns;"></tr>
        </table>
      </div>



      <div fxFlex="20">
        <h1>Details</h1>
        <div>
          <div class="image-preview w-100 bg-grey" #imagePreview [fxHide]="!(file && file.image)"></div>
          <div fxLayout="row" fxLayoutGap="1em" fxLayoutAlign="space-between center" *ngIf="file">
            <mat-form-field class="example-full-width">
              <input matInput placeholder="File link" #linkInput value="/public/files{{this.path}}/{{file.name}}">
            </mat-form-field>
            <button mat-icon-button matTooltip="Copy file link" (click)="copyLink(linkInput)">
              <mat-icon aria-label="Copy link">link</mat-icon>
            </button>
          </div>
          <div *ngIf="file">
            <p><strong>Path: </strong>{{this.path}}/</p>
            <p><strong>Filename: </strong>{{file.name}}</p>
            <p><strong>Mime type: </strong>{{file.mime}}</p>
            <p><strong>Size: </strong>{{file.size}}</p>
          </div>
        </div>



      </div>
    </section>

  `,
  styles: [`
  .image-preview {
    height: 240px;
  }
  `]
})
export class FilesComponent implements OnInit, OnDestroy {

  folderName = new FormControl('', [
    Validators.required,
    Validators.pattern('^\\w+$'),
  ]);

  componentDestroyed$: Subject<boolean> = new Subject();

  path: string;
  file: {
    name: string;
    mime: string;
    size: string;
    image?: string;
  };

  @ViewChild('imagePreview', { static: true }) imagePreview: any;
  @ViewChild('createFolder', { static: false }) createFolderButton: any;

  list: FileModel[] = [];
  dataSource = new MatTableDataSource(this.list);
  displayedColumns = ['folder', 'name', 'mime', 'size', 'options'];

  constructor(private actionsSubj: ActionsSubject,
              private snackBar: MatSnackBar,
              private filesStore: Store<FilesState>) { }

  ngOnInit() {
    this.filesStore.select(getPath)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(d => {
        this.path = d;
        this.loadFiles(this.path);
      });

    this.filesStore.select(getList)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        const d = data.sort((a, b) => {
          let vt = 0;
          let vn = 0;

          if (a.type < b.type) vt--;
          if (a.type > b.type) vt++;

          if (a.name < b.name) vn--;
          if (a.name > b.name) vn++;

          return vt | vn;
        });
        this.dataSource =  new MatTableDataSource(d);
      });

    this.folderName.valueChanges
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => data);

    this.actionsSubj
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data.type === FilesActionTypes.CreateDirSuccess) this.folderName.reset();
        if (data.type === FilesActionTypes.LoadFilesSuccess) this.file = undefined;
      });

    this.filesStore.select(getError)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data) this.snackBar.open(data, undefined, {duration: 3000});
      });
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  setFileSize(size) {
    if (!size) return;
    else {
      const s = +size;
      const k = (s < 100000) ? 1000 : 1000000;
      return `${(s / k).toString()} ${(k === 1000) ? 'Kb' : 'Mb'}`;
    }
  }

  onSelected(element) {
    if (element.type === 'directory') this.loadFiles(this.path + '/' + element.name);
    else {
      this.file = {
        name: element.name,
        mime: element.mime,
        size: this.setFileSize(element.size),
        image: (element.mime.includes('image') && !element.mime.includes('svg'))
          ? `${host}/public${this.path}/${element.name}` : undefined
      };
    }
    if (this.file && this.file.image) this.imagePreview.nativeElement.style.backgroundImage = `url(${this.file.image})`;
  }

  goUp() {
    if (this.path === '/') return;
    let p: any  = this.path;
    p = p.split('/');
    p.pop();
    p = p.join('/');
    this.loadFiles(p);
  }

  loadFiles(path) {
    this.filesStore.dispatch(new LoadFiles(path));
  }


  onFileSelected(e) {
    const data = new FormData();
    data.append('file', e.target.files[0]);
    data.append('dir', (!this.path.length) ? '/' : this.path);
    this.filesStore.dispatch(new UploadFile({parent: this.path, data}));
  }

  createFolder() {
    this.filesStore.dispatch(new CreateDir({
      parent: this.path,
      path: this.path + '/' + this.folderName.value
    }));
  }

  delete(type, name) {
    const snackBarRef = this.snackBar.openFromComponent(WarningSnackbarComponent, {
      data: 'You really want to delete this item?',
      duration: 3000,
      panelClass: 'bg-grey'
    });
    snackBarRef.onAction()
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(() => {
        const payload = {parent: this.path, path: this.path + '/' + name};
        if (type === 'directory') this.filesStore.dispatch(new DeleteDir(payload));
        else this.filesStore.dispatch(new DeleteFile(payload));
      });
  }

  copyLink(inputElement) {
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
  }
}
