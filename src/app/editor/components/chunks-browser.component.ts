import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {ActionsSubject, Store} from '@ngrx/store';
import {getSelectedBrowser, State as TagsState} from '../reducers/tags.reducer';
import {getCount, getRows, State as ChunksState} from '../reducers/chunks.reducer';
import {takeUntil} from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {ChunkModel} from '../models/chunks.model';
import {ChunksActionTypes, DeleteChunk, GetChunk, LoadChunks} from '../actions/chunks.actions';
import {DefGetItemsParams} from '../../utils/router.utils';
import {WarningSnackbarComponent} from '../../shared/components/warning-snackbar.component';
import {TagsActionTypes} from '../actions/tags.actions';

@Component({
  selector: 'app-chunks-browser',
  template: `
    <table mat-table [dataSource]="dataSource" matSort class="w-100">

      <ng-container matColumnDef="id">
        <th mat-header-cell class="w-200-px"  *matHeaderCellDef mat-sort-header> id </th>
        <td mat-cell class="cell-link w-200-px" *matCellDef="let element" (click)="loadChunk(element.id, false)"> {{element.id}} </td>
      </ng-container>

      <ng-container matColumnDef="description">
        <th mat-header-cell *matHeaderCellDef mat-sort-header> description </th>
        <td mat-cell *matCellDef="let element"> {{element.description}} </td>
      </ng-container>

      <ng-container matColumnDef="options">
        <th mat-header-cell class="w-100-px text-center"  *matHeaderCellDef> options </th>
        <td mat-cell class="w-100-px text-center" *matCellDef="let element">
          <button mat-icon-button (click)="loadChunk(element.id, true)">
            <mat-icon aria-label="Duplicate element">file_copy</mat-icon>
          </button>
          <button mat-icon-button color="warn" (click)="delete(element.id)">
            <mat-icon aria-label="Delete element">delete</mat-icon>
          </button>
        </td>
      </ng-container>

      <tr mat-header-row *matHeaderRowDef="displayedColumns; sticky: true"></tr>
      <tr mat-row *matRowDef="let row; columns: displayedColumns;"></tr>
    </table>
    <mat-paginator [length]="count"
                   [pageSize]="loadParams.limit"
                   [pageSizeOptions]="[5, 10, 25, 100]"
                   (page)="redefineParams($event)">
    </mat-paginator>
  `,
  styles: [``]
})
export class ChunksBrowserComponent implements OnInit, OnDestroy {

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  _data: ChunkModel[] = [];

  displayedColumns: string[] = ['id', 'description', 'options'];
  dataSource = new MatTableDataSource(this._data);

  componentDestroyed$: Subject<boolean> = new Subject();

  loadParams: DefGetItemsParams = {
    limit: '25',
    offset: '0',
    order: 'id',
    desc: 'false'
  };

  count = 0;

  constructor(private tagsStore: Store<TagsState>,
              private chunksStore: Store<ChunksState>,
              private actionsSubj: ActionsSubject,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {

    this.chunksStore.select(getRows)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        this.dataSource =  new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
      });

    this.chunksStore.select(getCount)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
         this.count = data;
      });

    this.tagsStore.select(getSelectedBrowser)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data) {
          if (data.length < 1) delete this.loadParams.filterTag;
          else this.loadParams.filterTag = data;
        }
      });

    this.actionsSubj
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data.type === ChunksActionTypes.DeleteChunkSuccess
          || data.type === TagsActionTypes.SetSelectedBrowserSuccess) this.loadChunks();
      });
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  public loadChunks() {
    this.chunksStore.dispatch(new LoadChunks(this.loadParams));
  }

  delete(id) {
    const snackBarRef = this.snackBar.openFromComponent(WarningSnackbarComponent, {
      data: 'You really want to delete this chunk?',
      duration: 3000,
      panelClass: 'bg-grey'
    });
    snackBarRef.onAction()
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(() => {
        this.chunksStore.dispatch(new DeleteChunk(id));
    });
  }

  redefineParams(event) {
    this.loadParams.limit = event.pageSize.toString();
    this.loadParams.offset = (event.pageSize * event.pageIndex).toString();
    this.loadChunks();
  }

  loadChunk(id, copy) {
    this.chunksStore.dispatch(new GetChunk({id, copy}));
  }
}
