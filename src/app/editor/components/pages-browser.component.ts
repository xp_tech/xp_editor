import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {Subject} from 'rxjs';
import {DefGetItemsParams} from '../../utils/router.utils';
import {PageModel} from '../models/pages.model';
import {takeUntil} from 'rxjs/operators';
import {getSetup, State as SettingsState} from '../../admin/reducers/settings.reducer';
import {State as PagesState, getRows as getPagesRows, getCount as getPagesCount} from '../reducers/pages.reducer';
import {State as TagsState, getSelectedBrowser} from '../reducers/tags.reducer';
import {State as ArticlesState, getRows as getArticlesRows, getCount as getArticlesCount} from '../reducers/articles.reducer';
import {ActionsSubject, Store} from '@ngrx/store';
import {DeletePage, GetPage, LoadPages, PagesActionTypes} from '../actions/pages.actions';
import {WarningSnackbarComponent} from 'src/app/shared/components/warning-snackbar.component';
import {ArticlesActionTypes, DeleteArticle, GetArticle, LoadArticles} from '../actions/articles.actions';
import {TagsActionTypes} from "../actions/tags.actions";

@Component({
  selector: 'app-pages-browser',
  template: `
    <div fxLayout="row" fxLayoutGap="1em" fxLayoutAlign="start center">
      <span class="mat-h3 mt-1">Filters:</span>
      <mat-form-field>
        <mat-label>Locale</mat-label>
        <mat-select #locSelect [(value)]="selectedLocale" (selectionChange)="setFilter(selectedLocale,'filterLocale')">
          <mat-option value="all">all</mat-option>
          <mat-option *ngFor="let i of locales" value="{{i}}">{{i}}</mat-option>
        </mat-select>
      </mat-form-field>
      <mat-form-field>
        <mat-label>Subdomains</mat-label>
        <mat-select #sdSelect [(value)]="selectedSubdomain" (selectionChange)="setFilter(selectedSubdomain,'filterSubdomain')">
          <mat-option value="all">all</mat-option>
          <mat-option *ngFor="let i of subdomains" value="{{i}}">{{i}}</mat-option>
        </mat-select>
      </mat-form-field>
      <mat-form-field>
        <mat-label>Published</mat-label>
        <mat-select #pubSelect [(value)]="selectedPublished" (selectionChange)="setFilter(selectedPublished,'filterPublished')">
          <mat-option value="all">all</mat-option>
          <mat-option value="true">published</mat-option>
          <mat-option value="false">not published</mat-option>
        </mat-select>
      </mat-form-field>
      
      <div>
        <button mat-icon-button (click)="redefineParams($event)">
          <mat-icon aria-label="Sort direction">sort_by_alpha</mat-icon>
        </button>
      </div>

    </div>

    <table mat-table [dataSource]="dataSource" matSort class="w-100">

      <ng-container matColumnDef="id">
        <th mat-header-cell class="w-150-px"  *matHeaderCellDef mat-sort-header> id </th>
        <td mat-cell class="cell-link w-100-px" *matCellDef="let element"
            (click)="loadPage(element.id, false)"
            [class.text-warn]="!(element.published)"> {{element.id}} </td>
      </ng-container>

      <ng-container matColumnDef="url">
        <th mat-header-cell class="w-200-px"  *matHeaderCellDef mat-sort-header> url </th>
        <td mat-cell class="cell-link w-200-px" *matCellDef="let element"
            (click)="loadPage(element.id, false)"
            [class.text-warn]="!(element.published)"> {{element.url}} </td>
      </ng-container>

      <ng-container matColumnDef="title">
        <th mat-header-cell *matHeaderCellDef mat-sort-header> title </th>
        <td mat-cell *matCellDef="let element" [class.text-warn]="!(element.published)"> {{element.title}} </td>
      </ng-container>

      <ng-container matColumnDef="locale">
        <th mat-header-cell class="w-50-px text-center"  *matHeaderCellDef> locale </th>
        <td mat-cell class="w-50-px text-center" [class.text-warn]="!(element.published)" *matCellDef="let element"> {{element.locale}} </td>
      </ng-container>

      <ng-container matColumnDef="subdomain">
        <th mat-header-cell class="w-150-px text-center"  *matHeaderCellDef> subdomain </th>
        <td mat-cell class="w-150-px text-center" [class.text-warn]="!(element.published)" *matCellDef="let element"> {{element.subdomain}} </td>
      </ng-container>

      <ng-container matColumnDef="options">
        <th mat-header-cell class="w-100-px text-center"  *matHeaderCellDef> options </th>
        <td mat-cell class="w-100-px text-center" *matCellDef="let element">
          <button mat-icon-button (click)="loadPage(element.id, true)">
            <mat-icon aria-label="Duplicate element">file_copy</mat-icon>
          </button>
          <button mat-icon-button color="warn" (click)="delete(element.id)">
            <mat-icon aria-label="Delete element">delete</mat-icon>
          </button>
        </td>
      </ng-container>

      <tr mat-header-row *matHeaderRowDef="displayedColumns; sticky: true"></tr>
      <tr mat-row *matRowDef="let row; columns: displayedColumns;"></tr>
    </table>
    <mat-paginator [length]="count"
                   [pageSize]="loadParams.limit"
                   [pageSizeOptions]="[5, 10, 25, 100]"
                   (page)="redefineParams($event)">
    </mat-paginator>
  `,
  styles: []
})
export class PagesBrowserComponent implements OnInit, OnDestroy {

  @Input() entity: 'pages' | 'articles';

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  _data: PageModel[] = [];

  displayedColumns: string[] = ['id', 'url', 'title', 'locale', 'subdomain', 'options'];

  dataSource = new MatTableDataSource(this._data);

  componentDestroyed$: Subject<boolean> = new Subject();

  loadParams: DefGetItemsParams = {
    limit: '25',
    offset: '0',
    order: 'id',
    desc: 'true'
  };

  count = 0;

  locales: string[];
  subdomains: string[];

  selectedLocale = 'all';
  selectedPublished = 'all';
  selectedSubdomain = 'all';

  constructor(private tagsStore: Store<TagsState>,
              private pagesStore: Store<PagesState>,
              private articlesStore: Store<ArticlesState>,
              private settingsStore: Store<SettingsState>,
              private actionsSubj: ActionsSubject,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.settingsStore.select(getSetup)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data) {
          this.locales = data.locales;
          this.subdomains = data.subdomains;
        }
      });

    let gr: any;
    if (this.entity === 'pages') gr = this.pagesStore.select(getPagesRows);
    if (this.entity === 'articles') gr = this.articlesStore.select(getArticlesRows);
    gr.pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        this.dataSource =  new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
      });

    let gc: any;
    if (this.entity === 'pages') gc = this.pagesStore.select(getPagesCount);
    if (this.entity === 'articles') gc = this.articlesStore.select(getArticlesCount);
    gc.pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        this.count = data;
      });

    this.tagsStore.select(getSelectedBrowser)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data) {
          if (data.length < 1) delete this.loadParams.filterTag;
          else this.loadParams.filterTag = data;
        }
      });

    this.actionsSubj
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data.type === PagesActionTypes.DeletePageSuccess
          || data.type === ArticlesActionTypes.DeleteArticleSuccess
          || data.type === TagsActionTypes.SetSelectedBrowserSuccess) this.loadPages();
      });
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  public loadPages() {
    if (this.entity === 'pages') this.pagesStore.dispatch(new LoadPages(this.loadParams));
    if (this.entity === 'articles') this.articlesStore.dispatch((new LoadArticles(this.loadParams)));
  }

  delete(id) {
    const snackBarRef = this.snackBar.openFromComponent(WarningSnackbarComponent, {
      data: `You really want to delete this ${this.entity.slice(0, -1)}?`,
      duration: 3000,
      panelClass: 'bg-grey'
    });
    snackBarRef.onAction()
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(() => {
        if (this.entity === 'pages') this.pagesStore.dispatch(new DeletePage(id));
        if (this.entity === 'articles') this.articlesStore.dispatch((new DeleteArticle(id)));
      });
  }

  redefineParams(event) {
    if (event.target && event.target.textContent === 'sort_by_alpha') {
      this.loadParams.desc = (this.loadParams.desc === 'false') ? 'true' : 'false';
    } else {
      this.loadParams.limit = event.pageSize.toString();
      this.loadParams.offset = (event.pageSize * event.pageIndex).toString();
    }
    this.loadPages();
  }

  setFilter(value, filter) {
    (value === 'all')
      ? delete this.loadParams[filter]
      : this.loadParams[filter] = value;
    this.loadPages();
  }

  loadPage(id, copy) {
    if (this.entity === 'pages') this.pagesStore.dispatch(new GetPage({id, copy}));
    if (this.entity === 'articles') this.articlesStore.dispatch((new GetArticle({id, copy})));
  }
}
