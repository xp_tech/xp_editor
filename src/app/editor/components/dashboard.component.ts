import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  template: `
    <section fxLayout="row" fxLayoutGap="2em">
      <div fxFlex="80">
        <h1>Welcome to XP editor v. 0.1.0!</h1>
      </div>

      <div fxFlex="20">
        <h1>Latest updates</h1>
      </div>
    </section>
  `,
  styles: []
})
export class DashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }



}
