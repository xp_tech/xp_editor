import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import * as ace from 'ace-builds';
import 'ace-builds/src-noconflict/mode-ejs';
import 'ace-builds/src-noconflict/mode-css';
import 'ace-builds/src-noconflict/theme-github';
import 'ace-builds/src-noconflict/ext-language_tools';
import 'ace-builds/src-noconflict/ext-beautify';

const INIT_CONTENT = '';
const THEME = 'ace/theme/github';

@Component({
  selector: 'app-ace-editor',
  template: `
    <div class="code-editor" #codeEditor></div>
    <div class="bg-grey">
       <button mat-button class="m-1" (click)="beautifyContent()">Beautify</button>
       <button mat-button class="m-1" (click)="setContent('')">Clear</button>
    </div>


  `,
  styles: []
})
export class AceEditorComponent implements OnInit {

  private codeEditor: ace.Ace.Editor;
  private editorBeautify; // beautify extension
  @ViewChild('codeEditor', { static: true }) private codeEditorElmRef: ElementRef;
  @Input() content: string;
  @Input() mode: 'ejs' | 'css';

  constructor() { }

  ngOnInit() {
    ace.require('ace/ext/language_tools');
    const element = this.codeEditorElmRef.nativeElement;
    const editorOptions = this.getEditorOptions();
    this.codeEditor = this.createCodeEditor(element, editorOptions);
    this.setContent(this.content || INIT_CONTENT);
    // hold reference to beautify extension
    this.editorBeautify = ace.require('ace/ext/beautify');
  }


  private createCodeEditor(element: Element, options: any): ace.Ace.Editor {
    const editor = ace.edit(element, options);
    editor.setTheme(THEME);
    editor.getSession().setMode(`ace/mode/${this.mode}`);
    editor.setShowFoldWidgets(true);
    return editor;
  }

  // missing propery on EditorOptions 'enableBasicAutocompletion' so this is a wolkaround still using ts
  private getEditorOptions(): Partial<ace.Ace.EditorOptions> & { enableBasicAutocompletion?: boolean; } {
    const basicEditorOptions: Partial<ace.Ace.EditorOptions> = {
      highlightActiveLine: true,
      minLines: 20,
      maxLines: 40// Infinity,
    };
    const extraEditorOptions = { enableBasicAutocompletion: true };
    return Object.assign(basicEditorOptions, extraEditorOptions);
  }

  /**
   * @returns - the current editor's content.
   */
  public getContent() {
    if (this.codeEditor) {
      return this.codeEditor.getValue();
    }
  }

  /**
   * @param content - set as the editor's content.
   */
  public setContent(content: string): void {
    if (this.codeEditor) {
      this.codeEditor.setValue(content);
    }
  }

  /**
   * @description
   *  beautify the editor content, rely on Ace Beautify extension.
   */
  public beautifyContent() {
    if (this.codeEditor && this.editorBeautify) {
      const session = this.codeEditor.getSession();
      this.editorBeautify.beautify(session);
    }
  }

  /**
   * @event OnContentChange - a proxy event to Ace 'change' event - adding additional data.
   * @param callback - receive the current content and 'change' event's original parameter.
   */
  public OnContentChange(callback: (content: string, delta: ace.Ace.Delta) => void): void {
    this.codeEditor.on('change', (delta) => {
      const content = this.codeEditor.getValue();
      callback(content, delta);
    });
  }

}
