import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {FormControl, Validators} from '@angular/forms';
import {ActionsSubject, Store} from '@ngrx/store';
import {getCount, getRows, getSelectedBrowser, getSelectedEditor, State as TagsState} from '../reducers/tags.reducer';
import {DeleteTag, GetTag, LoadTags, SetSelected, TagsActionTypes} from '../actions/tags.actions';
import {takeUntil} from 'rxjs/operators';
import {TagModel} from '../models/tag.model';
import { MatButtonToggleGroup } from '@angular/material/button-toggle';
import { MatSnackBar } from '@angular/material/snack-bar';
import {WarningSnackbarComponent} from '../../shared/components/warning-snackbar.component';

@Component({
  selector: 'app-tags-editor',
  template: `
    <div fxLayout="row">
      <div fxFlex>
        <mat-form-field class="w-100">
          <input matInput type="text" placeholder="Tag ID" [formControl]="tagIdFC" (keypress)="pressEnter($event, tagIdFC.value)">
          <mat-error *ngIf="tagIdFC.hasError('pattern') || tagIdFC.hasError('required')">
            Not a valid ID (letters and _ only)
          </mat-error>
          <mat-hint>Total tags created: {{count}}</mat-hint>
        </mat-form-field>

        <div class="bt-1 expander" (click)="expand = !expand">
          <mat-icon>{{(expand) ? 'expand_more' : 'expand_less'}}</mat-icon>
        </div>

        <mat-form-field class="w-100" [fxHide]="!(expand)">
          <textarea matInput cdkTextareaAutosize placeholder="Tag description"
                    [formControl]="tagDescriptionFC"></textarea>
        </mat-form-field>

      </div>
      <div>
        <button mat-icon-button class="mx-1 mt-2">
          <mat-icon aria-label="Add tag" (click)="addTag(tagIdFC.value)">add</mat-icon>
        </button>
      </div>

    </div>

    <div fxLayout="row">
      <div fxFlex class="flex-col">
        <mat-button-toggle-group #group="matButtonToggleGroup"
                                 (change)="setSelected(group.value)"
                                 multiple vertical class="w-100">

          <mat-button-toggle *ngFor="let item of rows; index as i" value="{{item.id}}"
                             matTooltip="{{item.description}}">
            <span>{{item.id}}</span>
            <mat-icon aria-label="Delete tag" class="delete" (click)="deleteTag(item.id, $event)">close</mat-icon>
          </mat-button-toggle>
        </mat-button-toggle-group>
      </div>
      <div>

      </div>

    </div>
  `,
  styles: [`
    .expander:hover {
      cursor: pointer
    }

    .flex-col {
      min-width: 180px;
    }

    mat-button-toggle {
        position: relative;
    }

    .delete {
      position: absolute;
      right: 0;
      display: none;
    }

    mat-button-toggle:hover .delete{
      display: inline-block !important;
    }

  `]
})
export class TagsEditorComponent implements OnInit, OnDestroy {

  @Input() entity: string;

  private _mode: string;

  @Input() set mode(value: string) {
    this._mode = value;
    this.groupRef.value = (value === 'browser') ? this.selectedBrowser : this.selectedEditor;
  }

  get mode(): string {
    return this._mode;
  }

  @ViewChild('group', { static: true }) groupRef: MatButtonToggleGroup;

  limit = '100';
  offset = '0';
  order = 'name';
  desc = 'false';

  rows: TagModel[];

  expand: boolean;

  group;

  modes = ['editor', 'browser'];
  selectedBrowser: string[];
  selectedEditor: string[];


  filterID: string;
  count: number;

  tagIdFC = new FormControl('', [
    Validators.required,
    Validators.pattern('^\\w+$'),
  ]);

  tagDescriptionFC = new FormControl('');

  componentDestroyed$: Subject<boolean> = new Subject();

  constructor(private tagsStore: Store<TagsState>,
              private actionsSubj: ActionsSubject,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {

    for (const i of this.modes) { this.setSelected([], i); }

    this.getAll();

    this.tagsStore.select(getRows)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => this.rows = data);

    this.tagsStore.select(getCount)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => this.count = data);

    this.tagsStore.select(getSelectedBrowser)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        this.selectedBrowser = data;
        if (this.mode === 'browser') { this.groupRef.value = data; }
      });

    this.tagsStore.select(getSelectedEditor)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        this.selectedEditor = data;
        if (this.mode === 'editor') { this.groupRef.value = data; }
      });

    this.actionsSubj
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data.type === TagsActionTypes.GetTagSuccess) {
          this.tagIdFC.reset();
          this.tagDescriptionFC.reset();
          this.getAll();
        }
        if (data.type === TagsActionTypes.DeleteTagSuccess) {
          this.getAll();
        }
      });
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  getAll(limit?, offset?, order?, desc?, filterID?) {
    const params = {
      entity: this.entity,
      limit: this.limit,
      offset: this.offset,
      order: this.order,
      desc: this.desc,
      filterID: this.filterID
    };
    this.tagsStore.dispatch(new LoadTags(params));

  }

  pressEnter(e, data) {
    if (e.key === 'Enter') this.addTag(data);
  }

  addTag(id) {
    if (this.tagIdFC.valid) {
      this.tagsStore.dispatch(new GetTag({entity: this.entity, id, description: this.tagDescriptionFC.value}));
    }

  }

  setSelected(val, mode = this.mode) {
    this.tagsStore.dispatch(new SetSelected({mode, data: val}));
  }

  deleteTag(id, event) {
    event.stopPropagation();

    const snackBarRef = this.snackBar.openFromComponent(WarningSnackbarComponent, {
      data: 'You really want to delete this tag?',
      duration: 3000,
      panelClass: 'bg-grey'
    });

    snackBarRef.onAction()
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(() => {
        const lists = [this.selectedEditor, this.selectedBrowser];

        for (const il in lists) {
          const gVal = lists[il];
          for (let i = 0; i < gVal.length; i++) {
            if ( gVal[i] === id) {
              gVal.splice(i, 1);
              i--;
            }
          }
          this.setSelected(gVal, this.modes[il]);
        }

        this.tagsStore.dispatch(new DeleteTag({entity: this.entity, id}));
      });

  }

}
