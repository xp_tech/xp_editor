import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CreatePage, PagesActionTypes, ResetPage, UpdatePage} from '../actions/pages.actions';
import {AceEditorComponent} from './ace-editor.component';
import {PageModel} from '../models/pages.model';
import {ArticleModel} from '../models/articles.model';
import {SetSelected} from '../actions/tags.actions';
import {ActionsSubject, Store} from '@ngrx/store';
import {getSelectedEditor, State as TagsState} from '../reducers/tags.reducer';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {State as PagesState} from '../reducers/pages.reducer';
import {State as ArticlesState} from '../reducers/articles.reducer';
import {ArticlesActionTypes, CreateArticle, ResetArticle, UpdateArticle} from '../actions/articles.actions';
import { MatButtonToggleGroup } from '@angular/material/button-toggle';
import {getSetup, State as SettingsState} from '../../admin/reducers/settings.reducer';
import {host} from '../../utils/router.utils';
import {getUser, State as UserState} from '../../user/user.reducer';

@Component({
  selector: 'app-pages-editor',
  templateUrl: './pages-editor.component.html',
  styles: [`
    .va-fix mat-icon {
      vertical-align: baseline !important;
    }
  `]
})
export class PagesEditorComponent implements OnInit, OnDestroy {

  @Input() entity: 'pages' | 'articles';

  userRole:string;

  pageAttributes = new FormGroup({
    id: new FormControl('', [
      Validators.required,
      Validators.pattern('^\\w+$'),
    ]),

    url: new FormControl('', [
      Validators.required,
    ]),
    published: new FormControl(),
    title: new FormControl(),
    description: new FormControl(),
    keywords: new FormControl(),
    header: new FormControl(),
    summary: new FormControl(),
    template: new FormControl('', (this.entity === 'articles') ? [
      Validators.required,
      Validators.pattern('^\\w+$'),
    ] : []),

    locale: new FormControl('', [
      Validators.required,
    ]),

    subdomain: new FormControl('', [
      Validators.required,
    ]),

    og_image_url: new FormControl('', [
      Validators.required,
    ]),

    og_image_width: new FormControl('', [
      Validators.required,
    ]),

    og_image_height: new FormControl('', [
      Validators.required,
    ]),
  });

  saveDisabled: boolean;
  new = true;

  locales: string[] = [];
  subdomains: string[] = [];

  componentDestroyed$: Subject<boolean> = new Subject();

  @ViewChild('toolsSelectorGroup', { static: true }) toolsSelectorGroup: MatButtonToggleGroup;
  @ViewChild('imagePreview', { static: true }) imagePreview: any;
  @ViewChild('editor', { static: true }) editor: AceEditorComponent;
  @ViewChild('editorCSS', { static: true }) editorCSS: AceEditorComponent;

  eName: string;

  private _data: PageModel | ArticleModel;
  @Input() set data(value: PageModel | ArticleModel) {
    this._data = value;
    const pa = this.pageAttributes;
    const ogDef = {
      url: '/images/og-image.jpg',
      width: 1200,
      height: 630
    };
    const mainAttr = ['id', 'url', 'title', 'description', 'keywords'];
    const articleAttr = ['header', 'summary', 'template'];

    for (const i of mainAttr.concat(articleAttr)) {
      if (this.entity === 'pages' && articleAttr.includes(i)) break;
      const a = pa.get(i);
      a.valueChanges
        .pipe(takeUntil(this.componentDestroyed$))
        .subscribe(d => {
          this._data[i] = d;
          this.setSaveDisabled();
        });
      (this._data[i]) ? a.setValue(this._data[i]) : a.reset();
    }

    for (const i of ['og_image_url', 'og_image_width', 'og_image_height']) {
      const a = pa.get(i);
      const n = i.split('_')[2];
      const og = this._data.ogImage;
      a.valueChanges
        .pipe(takeUntil(this.componentDestroyed$))
        .subscribe(d => {
          this._data.ogImage[n] = d;
          this.setSaveDisabled();
          if (n === 'url') {
            const url = (d.startsWith('/')) ? `${host}${d}` : d;
            this.imagePreview.nativeElement.style.backgroundImage = `url(${url})`;
          }
        });
      a.setValue((og && og[n]) ? og[n] : ogDef[n]);
    }

    for (const i of ['locale', 'subdomain']) {
      const a = pa.get(i);
      a.valueChanges
        .pipe(takeUntil(this.componentDestroyed$))
        .subscribe(d => {
          this._data[i] = d;
          this.setSaveDisabled();
        });
      a.setValue((this._data[i]) ?
        this._data[i]
        : (i === 'locale') ? this.locales[0] : this.subdomains [0]
      );
    }

    const pub = pa.get('published');
    pub.valueChanges
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(d => {
        this._data.published = d.toString();
        this.setSaveDisabled();
      });
    pub.setValue(!!(this._data.published));

    this.new = !(this._data.id);
    if (!this.new) pa.get('id').disable();
    else pa.get('id').enable();

    this.editor.setContent((this._data.body) ? this._data.body : '');
    if (this.entity === 'pages') {
      const css = (this._data as PageModel).css;
      this.editorCSS.setContent((css) ? css : '');
    }

    if (this._data.tags) {
      this.tagsStore.dispatch(new SetSelected({
        mode: 'editor',
        data: this._data.tags
      }));
    }
  }

  get data(): PageModel | ArticleModel {
    return this._data;
  }

  constructor(private actionsSubj: ActionsSubject,
              private tagsStore: Store<TagsState>,
              private settingsState: Store<SettingsState>,
              private userStore: Store<UserState>,
              private articlesStore: Store<ArticlesState>,
              private pagesStore: Store<PagesState>) {
  }

  ngOnInit() {
    if (this.entity === 'pages') this.eName = 'Page';
    if (this.entity === 'articles') this.eName = 'Article';

    this.userStore.select(getUser)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data && data.role) { this.userRole = data.role; }
      });


    this.settingsState.select(getSetup)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data) {
          this.locales = data.locales;
          this.subdomains = data.subdomains;
        }
      });

    this.tagsStore.select(getSelectedEditor)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(d => {
        this._data.tags = d;
        if (this._data.id) this.save();
      });

    this.actionsSubj
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data.type === PagesActionTypes.CreatePageSuccess
          || data.type === ArticlesActionTypes.CreateArticleSuccess) {
          this.new = false;
          this.pageAttributes.get('id').disable();
        }
      });
  }

  ngOnDestroy(): void {
    this.createNew();
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  setActiveBody(event) {
    this._data.body = (event.target.value && event.target.value !== '\n\n')
      ? this.editor.getContent() : undefined;
    this.setSaveDisabled();
    this.save();
  }

  setActiveCSS(event) {
    if (this.entity === 'pages') {
      (this._data as PageModel).css = (event.target.value && event.target.value !== '\n\n')
        ? this.editorCSS.getContent() : undefined;
      this.save();
    }
  }

  setSaveDisabled() {
    this.saveDisabled = !(this.pageAttributes.valid && this._data.body);
  }

  createNew() {
    if (this.entity === 'pages') this.pagesStore.dispatch(new ResetPage());
    if (this.entity === 'articles') this.articlesStore.dispatch(new ResetArticle());
  }

  save() {
    if (this.saveDisabled) return;
    if (this.new) {
      if (this.entity === 'pages') this.pagesStore.dispatch(new CreatePage(this._data as PageModel));
      if (this.entity === 'articles') this.articlesStore.dispatch(new CreateArticle(this._data as ArticleModel));
    } else {
      if (this.entity === 'pages') this.pagesStore.dispatch(new UpdatePage(this._data as PageModel));
      if (this.entity === 'articles') this.articlesStore.dispatch(new UpdateArticle(this._data as ArticleModel));

    }
  }

}
