import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ActionsSubject, Store} from '@ngrx/store';
import {PagesActionTypes} from '../actions/pages.actions';
import {PagesBrowserComponent} from './pages-browser.component';
import {State as SettingsState} from '../../admin/reducers/settings.reducer';
import {LoadSettings} from '../../admin/actions/settings.actions';
import {getActive, State as PagesState} from '../reducers/pages.reducer';
import {PageModel} from '../models/pages.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import {getError} from '../reducers/chunks.reducer';

@Component({
  selector: 'app-pages',
  templateUrl: './page-tools.html',
  styles: []
})
export class PagesComponent implements OnInit, OnDestroy {

  selectedIndex = 1;
  data: PageModel;
  entity: 'pages' | 'articles' = 'pages';

  componentDestroyed$: Subject<boolean> = new Subject();

  @ViewChild('browser', { static: true }) browser: PagesBrowserComponent;

  constructor(private actionsSubj: ActionsSubject,
              private snackBar: MatSnackBar,
              private pagesStore: Store<PagesState>,
              private settingsStore: Store<SettingsState>) { }

  ngOnInit() {
    this.settingsStore.dispatch(new LoadSettings());

    this.pagesStore.select(getActive)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => this.data = data);

    this.actionsSubj
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data.type === PagesActionTypes.GetPage && this.selectedIndex === 1) {
          this.selectedIndex = 0;
        }
      });

    this.pagesStore.select(getError)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data) this.snackBar.open(data, undefined, {duration: 3000});
      });
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  selectedTabChange() {
    if (this.selectedIndex === 1) this.browser.loadPages();
  }

}
