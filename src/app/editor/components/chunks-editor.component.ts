import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {ActionsSubject, Store} from '@ngrx/store';
import {getSelectedEditor, State as TagsState} from '../reducers/tags.reducer';
import {getActive, State as ChunksState} from '../reducers/chunks.reducer';
import {ChunkModel} from '../models/chunks.model';
import {takeUntil} from 'rxjs/operators';
import {SetSelected} from '../actions/tags.actions';
import {ChunksActionTypes, CreateChunk, ResetChunk, UpdateChunk} from '../actions/chunks.actions';
import {AceEditorComponent} from './ace-editor.component';

@Component({
  selector: 'app-chunks-editor',
  template: `
    <div fxLayout="row" fxLayoutGap="1em" [formGroup]="chunkAttributes">
      <div fxFlex="40">
        <button mat-mini-fab class="mx-1" (click)="createNew()">
          <mat-icon aria-label="Create new">add</mat-icon>
        </button>
        <button mat-mini-fab class="mx-1"
                (click)="save()"
                disabled="{{saveDisabled}}">
          <mat-icon aria-label="Save data">save</mat-icon>
        </button>
        <mat-form-field>
          <input #id matInput type="text" placeholder="Chunk ID" required
                 maxlength="64"
                 (focusout)="save()"
                 formControlName="id">
          <mat-error
            *ngIf="chunkAttributes.get('id').hasError('pattern') || chunkAttributes.get('id').hasError('required')">
            Not a valid ID (letters and _ only)
          </mat-error>
          <mat-hint align="end">{{id.value.length}} / 64</mat-hint>
        </mat-form-field>
      </div>
      <mat-form-field fxFlex="60">
        <textarea #description matInput cdkTextareaAutosize placeholder="Chunk description"
                  maxlength="255"
                  (focusout)="save()"
                  formControlName="description"></textarea>
        <mat-hint align="end">{{description.value.length}} / 255</mat-hint>
      </mat-form-field>
    </div>
    <app-ace-editor #editor [content]="active.body" (focusout)="setActiveBody($event)" mode="ejs"></app-ace-editor>
  `,
  styles: []
})
export class ChunksEditorComponent implements OnInit, OnDestroy {

  chunkAttributes = new FormGroup({
    id: new FormControl('', [
      Validators.required,
      Validators.pattern('^\\w+$'),
    ]),

    description: new FormControl('')
  });

  active: ChunkModel;
  saveDisabled = true;
  new = true;

  @ViewChild('editor', { static: true }) editor: AceEditorComponent;

  componentDestroyed$: Subject<boolean> = new Subject();

  constructor(private actionsSubj: ActionsSubject,
              private tagsStore: Store<TagsState>,
              private chunksStore: Store<ChunksState>) {
  }

  ngOnInit() {

    this.chunkAttributes.get('id').valueChanges
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        this.active.id = data;
        this.setSaveDisabled();
      });

    this.chunkAttributes.get('description').valueChanges
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => this.active.description = data);

    this.chunksStore.select(getActive)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        this.active = data;
        const idFC = this.chunkAttributes.get('id');

        if (data.id) idFC.setValue(this.active.id);
        else idFC.reset();

        this.chunkAttributes.get('description').setValue(this.active.description);
        this.editor.setContent((data.body) ? this.active.body : '');

        this.new = (!data.id);
        if (!this.new) this.chunkAttributes.get('id').disable();
        else this.chunkAttributes.get('id').enable();

        if (data.tags) { this.tagsStore.dispatch(new SetSelected({
          mode: 'editor',
          data: data.tags
        }));
        }
      });


    this.tagsStore.select(getSelectedEditor)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        this.active.tags = data;
        this.save();
      });

    this.actionsSubj
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data.type === ChunksActionTypes.CreateChunkSuccess) {
          this.new = false;
          this.chunkAttributes.get('id').disable();
        }
      });
  }

  ngOnDestroy(): void {
    this.createNew();
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  setActiveBody(event) {
    this.active.body = (event.target.value && event.target.value !== '\n\n')
      ? this.editor.getContent() : undefined;
    this.setSaveDisabled();
    this.save();
  }

  setSaveDisabled() {
    this.saveDisabled = !(
      this.chunkAttributes.valid
      && this.active.body);
  }

  save() {
    if (this.saveDisabled) return;

    if (this.new) this.chunksStore.dispatch(new CreateChunk(this.active));
    else this.chunksStore.dispatch(new UpdateChunk(this.active));
  }

  createNew() {
    this.chunksStore.dispatch(new ResetChunk());
  }

}
