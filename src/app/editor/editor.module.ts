import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../material/material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {StoreModule} from '@ngrx/store';
import * as fromTags from './reducers/tags.reducer';
import * as fromChunks from './reducers/chunks.reducer';
import * as fromPages from './reducers/pages.reducer';
import * as fromArticles from './reducers/articles.reducer';
import * as fromFiles from './reducers/files.reducer';

import {SharedModule} from '../shared/shared.module';

import {EffectsModule} from '@ngrx/effects';
import {TagsEffects} from './effects/tags.effects';
import {ChunksEffects} from './effects/chunks.effects';
import {PagesEffects} from './effects/pages.effects';
import {ArticlesEffects} from './effects/articles.effects';
import {FilesEffects} from './effects/files.effects';

import {DashboardComponent} from './components/dashboard.component';
import {AceEditorComponent} from './components/ace-editor.component';
import {ChunksComponent} from './components/chunks.component';
import {ChunksEditorComponent} from './components/chunks-editor.component';
import {TagsEditorComponent} from './components/tags-editor.component';
import {ChunksBrowserComponent} from './components/chunks-browser.component';
import { PagesComponent } from './components/pages.component';
import { ArticlesComponent } from './components/articles.component';
import { TestsComponent } from './components/tests.component';
import { CoursesComponent } from './components/courses.component';
import { FilesComponent } from './components/files.component';
import { PagesBrowserComponent } from './components/pages-browser.component';
import { PagesEditorComponent } from './components/pages-editor.component';
import {AdminModule} from '../admin/admin.module';

@NgModule({
  declarations: [
    DashboardComponent,
    AceEditorComponent,
    ChunksComponent,
    ChunksEditorComponent,
    TagsEditorComponent,
    ChunksBrowserComponent,
    ChunksBrowserComponent,
    PagesComponent,
    ArticlesComponent,
    TestsComponent,
    CoursesComponent,
    FilesComponent,
    PagesBrowserComponent,
    PagesEditorComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    AdminModule,
    StoreModule.forFeature('tagsState', fromTags.reducer),
    StoreModule.forFeature('chunksState', fromChunks.reducer),
    StoreModule.forFeature('pagesState', fromPages.reducer),
    StoreModule.forFeature('articlesState', fromArticles.reducer),
    StoreModule.forFeature('filesState', fromFiles.reducer),
    EffectsModule.forFeature([
      TagsEffects,
      ChunksEffects,
      PagesEffects,
      ArticlesEffects,
      FilesEffects
    ])
  ],
})
export class EditorModule {
}
