import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import {
  ChunksActionTypes,
  CreateChunk,
  CreateChunkFailure,
  CreateChunkSuccess,
  ResetChunkSuccess,
  UpdateChunk,
  UpdateChunkSuccess,
  UpdateChunkFailure,
  DeleteChunk,
  DeleteChunkSuccess,
  DeleteChunkFailure,
  GetChunk,
  GetChunkSuccess, GetChunkFailure, LoadChunks, LoadChunksSuccess, LoadChunksFailure
} from '../actions/chunks.actions';
import {catchError, map, switchMap} from 'rxjs/operators';
import {ChunksService} from '../services/chunks.service';
import {ChunkModel, LoadedChunksModel} from '../models/chunks.model';

@Injectable()
export class ChunksEffects {

  activeChunk;

  constructor(private actions$: Actions,
              private chunksService: ChunksService) {}


  @Effect()
  resetChunk: Observable<Action> = this.actions$
    .pipe(
      ofType(ChunksActionTypes.ResetChunk),
      switchMap(action => of(new ResetChunkSuccess()))
    );

  @Effect()
  loadChunks: Observable<Action> = this.actions$
    .pipe(
      ofType(ChunksActionTypes.LoadChunks),
      switchMap((action: LoadChunks) => this.chunksService.getAll(action.payload)
        .pipe(
          map((data: LoadedChunksModel) => new LoadChunksSuccess(data)),
          catchError(error => of(new LoadChunksFailure(error)))
        )
      )
    );


  @Effect()
  getChunk: Observable<Action> = this.actions$
    .pipe(
      ofType(ChunksActionTypes.GetChunk),
      switchMap((action: GetChunk) => {
        const c = action.payload.copy;
        return this.chunksService.get(action.payload.id)
            .pipe(
              map((data: ChunkModel) => {
                const d = data;
                if (c) d.id = undefined;
                return new GetChunkSuccess(d);
              }),
              catchError(error => of(new GetChunkFailure(error)))
            );
        }
      )
    );


  @Effect()
  createChunk: Observable<Action> = this.actions$
    .pipe(
      ofType(ChunksActionTypes.CreateChunk),
      switchMap((action: CreateChunk) => this.chunksService.add(action.payload)
        .pipe(
          map((data: ChunkModel) => {
            if (!data.tags) data.tags = [];
            return new CreateChunkSuccess(data);
          }),
          catchError(error => of(new CreateChunkFailure(error)))
        ))
    );

  @Effect()
  updateChunk: Observable<Action> = this.actions$
    .pipe(
      ofType(ChunksActionTypes.UpdateChunk),
      switchMap((action: UpdateChunk) => {
        this.activeChunk = action.payload;
        return this.chunksService.update(this.activeChunk)
          .pipe(
            map((data: ChunkModel) => (data)
              ? new UpdateChunkSuccess(this.activeChunk)
              : new UpdateChunkFailure('Unknown error')),
            catchError(error => of(new UpdateChunkFailure(error)))
          );
      })
    );

  @Effect()
  deleteChunk: Observable<Action> = this.actions$
    .pipe(
      ofType(ChunksActionTypes.DeleteChunk),
      switchMap((action: DeleteChunk) => this.chunksService.delete(action.payload)
        .pipe(
          map(() => new DeleteChunkSuccess()),
          catchError(error => of(new DeleteChunkFailure(error)))
        )
      )
    );


}
