import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import {
  ArticlesActionTypes,
  CreateArticle,
  CreateArticleFailure,
  CreateArticleSuccess,
  ResetArticleSuccess,
  UpdateArticle,
  UpdateArticleSuccess,
  UpdateArticleFailure,
  DeleteArticle,
  DeleteArticleSuccess,
  DeleteArticleFailure,
  GetArticle,
  GetArticleSuccess, GetArticleFailure, LoadArticles, LoadArticlesSuccess, LoadArticlesFailure
} from '../actions/articles.actions';
import {catchError, map, switchMap} from 'rxjs/operators';
import {ArticlesService} from '../services/articles.service';
import {ArticleModel, LoadedArticlesModel} from '../models/articles.model';

@Injectable()
export class ArticlesEffects {

  activeArticle;

  constructor(private actions$: Actions,
              private articlesService: ArticlesService) {}


  @Effect()
  resetArticle: Observable<Action> = this.actions$
    .pipe(
      ofType(ArticlesActionTypes.ResetArticle),
      switchMap(action => of(new ResetArticleSuccess()))
    );

  @Effect()
  loadArticles: Observable<Action> = this.actions$
    .pipe(
      ofType(ArticlesActionTypes.LoadArticles),
      switchMap((action: LoadArticles) => this.articlesService.getAll(action.payload)
        .pipe(
          map((data: LoadedArticlesModel) => new LoadArticlesSuccess(data)),
          catchError(error => of(new LoadArticlesFailure(error)))
        )
      )
    );


  @Effect()
  getArticle: Observable<Action> = this.actions$
    .pipe(
      ofType(ArticlesActionTypes.GetArticle),
      switchMap((action: GetArticle) => {
          const c = action.payload.copy;
          return this.articlesService.get(action.payload.id)
            .pipe(
              map((data: ArticleModel) => {
                const d = data;
                if (c) d.id = undefined;
                return new GetArticleSuccess(d);
              }),
              catchError(error => of(new GetArticleFailure(error)))
            );
        }
      )
    );


  @Effect()
  createArticle: Observable<Action> = this.actions$
    .pipe(
      ofType(ArticlesActionTypes.CreateArticle),
      switchMap((action: CreateArticle) => this.articlesService.add(action.payload)
        .pipe(
          map((data: ArticleModel) => {
            if (!data.tags) data.tags = [];
            return new CreateArticleSuccess(data);
          }),
          catchError(error => of(new CreateArticleFailure(error)))
        ))
    );

  @Effect()
  updateArticle: Observable<Action> = this.actions$
    .pipe(
      ofType(ArticlesActionTypes.UpdateArticle),
      switchMap((action: UpdateArticle) => {
        this.activeArticle = action.payload;
        return this.articlesService.update(this.activeArticle)
          .pipe(
            map((data: ArticleModel) => (data)
              ? new UpdateArticleSuccess(this.activeArticle)
              : new UpdateArticleFailure('Unknown error')),
            catchError(error => of(new UpdateArticleFailure(error)))
          );
      })
    );

  @Effect()
  deleteArticle: Observable<Action> = this.actions$
    .pipe(
      ofType(ArticlesActionTypes.DeleteArticle),
      switchMap((action: DeleteArticle) => this.articlesService.delete(action.payload)
        .pipe(
          map(() => new DeleteArticleSuccess()),
          catchError(error => of(new DeleteArticleFailure(error)))
        )
      )
    );


}
