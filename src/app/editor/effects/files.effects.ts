import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import {FilesService} from '../services/files.service';
import {
  CreateDir,
  CreateDirSuccess, DeleteDir, DeleteDirFailure, DeleteDirSuccess, DeleteFile, DeleteFileFailure, DeleteFileSuccess,
  FilesActionTypes,
  LoadFiles,
  LoadFilesFailure,
  LoadFilesSuccess, UploadFile, UploadFileFailure, UploadFileSuccess
} from '../actions/files.actions';
import {FileModel} from '../models/files.model';
import {CreateChunkFailure} from '../actions/chunks.actions';

@Injectable()
export class FilesEffects {


  constructor(private actions$: Actions,
              private filesService: FilesService) {}


  @Effect()
  loadFiles: Observable<Action> = this.actions$
    .pipe(
      ofType(FilesActionTypes.LoadFiles),
      switchMap((action: LoadFiles) => {
        const p = action.payload;
        return this.filesService.loadDir(p)
            .pipe(
              map((data: FileModel[]) => new LoadFilesSuccess({path: p, list: data})),
              catchError(error => of(new LoadFilesFailure(error)))
            );
        }
      )
    );


  @Effect()
  createDir: Observable<any> = this.actions$
    .pipe(
      ofType(FilesActionTypes.CreateDir),
      switchMap((action: CreateDir) => {
        const p = action.payload;
        return this.filesService.createDir(p.path)
          .pipe(
            switchMap(_ => [new CreateDirSuccess(), new LoadFiles(p.parent)] ),
            catchError(err => of(new CreateChunkFailure(err)))
          );
      })
    );

  @Effect()
  deleteDir: Observable<any> = this.actions$
    .pipe(
      ofType(FilesActionTypes.DeleteDir),
      switchMap((action: DeleteDir) => {
        const p = action.payload;
        return this.filesService.deleteDir(p.path)
          .pipe(
            switchMap(_ => [new DeleteDirSuccess(), new LoadFiles(p.parent)] ),
            catchError(err => of(new DeleteDirFailure(err)))
          );
      })
    );

  @Effect()
  uploadFile: Observable<any> = this.actions$
    .pipe(
      ofType(FilesActionTypes.UploadFile),
      switchMap((action: UploadFile) => {
        const p = action.payload;
        return this.filesService.uploadFile(p.data)
          .pipe(
            switchMap(_ => [new UploadFileSuccess(), new LoadFiles(p.parent)] ),
            catchError(err => of(new UploadFileFailure(err)))
          );
      })
    );

  @Effect()
  deleteFile: Observable<any> = this.actions$
    .pipe(
      ofType(FilesActionTypes.DeleteFile),
      switchMap((action: DeleteFile) => {
        const p = action.payload;
        return this.filesService.deleteFile(p.path)
          .pipe(
            switchMap(_ => [new DeleteFileSuccess(), new LoadFiles(p.parent)] ),
            catchError(err => of(new DeleteFileFailure(err)))
          );
      })
    );

}
