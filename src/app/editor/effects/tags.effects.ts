import { Injectable } from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {TagsService} from '../services/tags.service';
import {Observable, of} from 'rxjs';
import {Action} from '@ngrx/store';
import {
  GetTag, GetTagFailure,
  GetTagSuccess,
  LoadTags,
  LoadTagsFailure,
  LoadTagsSuccess, SetSelected, SetSelectedBrowserSuccess, SetSelectedEditorSuccess,
  TagsActionTypes,
  DeleteTagSuccess,
  DeleteTagFailure, DeleteTag
} from '../actions/tags.actions';
import {catchError, map, switchMap} from 'rxjs/operators';
import {LoadedTagsModel, TagModel} from '../models/tag.model';
import {UserService} from '../../user/user.service';



@Injectable()
export class TagsEffects {

  constructor(private actions$: Actions,
              private tagsService: TagsService,
              private userService: UserService) {}

  @Effect()
  loadTags: Observable<Action> = this.actions$
    .pipe(
      ofType(TagsActionTypes.LoadTags),
      switchMap((action: LoadTags) => {
        const pl = action.payload;
        return this.tagsService.getAll(pl.entity, {
          limit: pl.limit,
          offset: pl.offset,
          order: pl.order,
          desc: pl.desc,
          filterID: pl.filterID
        }).pipe(
          map ((data: LoadedTagsModel) => new LoadTagsSuccess(data)),
          catchError(error => of(new LoadTagsFailure(error)))
        );
      })
    );

  @Effect()
  getTag: Observable<Action> = this.actions$
    .pipe(
      ofType(TagsActionTypes.GetTag),
      switchMap((action: GetTag) => {
        const pl = action.payload;
        return this.tagsService.get(pl.entity, pl.id, pl.description)
          .pipe(
            map((data: TagModel) => new GetTagSuccess()),
            catchError(error => of(new GetTagFailure(error)))
          );
      })
    );

  @Effect()
  setSelected: Observable<Action> = this.actions$
    .pipe(
      ofType(TagsActionTypes.SetSelected),
      switchMap((action: SetSelected) => of(action.payload)),
      map((payload: any) => (payload.mode === 'browser')
        ?  new SetSelectedBrowserSuccess(payload.data)
        : new SetSelectedEditorSuccess(payload.data)
      )
    );

  @Effect()
  deleteTag: Observable<Action> = this.actions$
    .pipe(
      ofType(TagsActionTypes.DeleteTag),
      switchMap((action: DeleteTag) => {
        const pl = action.payload;
        return this.tagsService.delete(pl.entity, pl.id)
          .pipe(
            map((data) => new DeleteTagSuccess()),
            catchError(error => of(new DeleteTagFailure(error)))
          );
      })
    );

}
