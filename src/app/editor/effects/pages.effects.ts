import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import {
  PagesActionTypes,
  CreatePage,
  CreatePageFailure,
  CreatePageSuccess,
  ResetPageSuccess,
  UpdatePage,
  UpdatePageSuccess,
  UpdatePageFailure,
  DeletePage,
  DeletePageSuccess,
  DeletePageFailure,
  GetPage,
  GetPageSuccess, GetPageFailure, LoadPages, LoadPagesSuccess, LoadPagesFailure
} from '../actions/pages.actions';
import {catchError, map, switchMap} from 'rxjs/operators';
import {PagesService} from '../services/pages.service';
import {PageModel, LoadedPagesModel} from '../models/pages.model';

@Injectable()
export class PagesEffects {

  activePage;

  constructor(private actions$: Actions,
              private pagesService: PagesService) {}


  @Effect()
  resetPage: Observable<Action> = this.actions$
    .pipe(
      ofType(PagesActionTypes.ResetPage),
      switchMap(action => of(new ResetPageSuccess()))
    );

  @Effect()
  loadPages: Observable<Action> = this.actions$
    .pipe(
      ofType(PagesActionTypes.LoadPages),
      switchMap((action: LoadPages) => this.pagesService.getAll(action.payload)
        .pipe(
          map((data: LoadedPagesModel) => new LoadPagesSuccess(data)),
          catchError(error => of(new LoadPagesFailure(error)))
        )
      )
    );


  @Effect()
  getPage: Observable<Action> = this.actions$
    .pipe(
      ofType(PagesActionTypes.GetPage),
      switchMap((action: GetPage) => {
        const c = action.payload.copy;
        return this.pagesService.get(action.payload.id)
            .pipe(
              map((data: PageModel) => {
                const d = data;
                if (c) d.id = undefined;
                return new GetPageSuccess(d);
              }),
              catchError(error => of(new GetPageFailure(error)))
            );
        }
      )
    );


  @Effect()
  createPage: Observable<Action> = this.actions$
    .pipe(
      ofType(PagesActionTypes.CreatePage),
      switchMap((action: CreatePage) => this.pagesService.add(action.payload)
        .pipe(
          map((data: PageModel) => {
            if (!data.tags) data.tags = [];
            return new CreatePageSuccess(data);
          }),
          catchError(error => of(new CreatePageFailure(error)))
        ))
    );

  @Effect()
  updatePage: Observable<Action> = this.actions$
    .pipe(
      ofType(PagesActionTypes.UpdatePage),
      switchMap((action: UpdatePage) => {
        this.activePage = action.payload;
        return this.pagesService.update(this.activePage)
          .pipe(
            map((data: PageModel) => (data)
              ? new UpdatePageSuccess(this.activePage)
              : new UpdatePageFailure('Unknown error')),
            catchError(error => of(new UpdatePageFailure(error)))
          );
      })
    );

  @Effect()
  deletePage: Observable<Action> = this.actions$
    .pipe(
      ofType(PagesActionTypes.DeletePage),
      switchMap((action: DeletePage) => this.pagesService.delete(action.payload)
        .pipe(
          map(() => new DeletePageSuccess()),
          catchError(error => of(new DeletePageFailure(error)))
        )
      )
    );


}
