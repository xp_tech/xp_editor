import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DefGetItemsParams, Server} from '../../utils/router.utils';
import {PageModel} from '../models/pages.model';

@Injectable({
  providedIn: 'root'
})
export class PagesService {

  constructor(private http: HttpClient) { }

  getAll(params: DefGetItemsParams) {
    const options = {
      params: {
        limit: params.limit || '20',
        offset: params.offset || '0',
        order: params.order || 'updatedAt',
        desc: params.desc || 'true',
      },
      withCredentials: true
    };

    for (const i of ['filterID', 'filterURL', 'filterLocale', 'filterSubdomain', 'filterPublished']) {
      if (params[i]) (options.params as any)[i] = params[i];
    }
    if (params.filterTag) (options.params as any).filterTag = JSON.stringify(params.filterTag);

    return this.http.get(Server.api('editor/pages'), options);
  }

  get(id: string) {
    return this.http.get(Server.api(`editor/pages/${id}`), {withCredentials: true});
  }

  add(item: PageModel) {
    return this.http.post(Server.api(`editor/pages/${item.id}`),
      item,
      {withCredentials: true});
  }

  update(item: PageModel) {
    return this.http.put(Server.api(`editor/pages/${item.id}`),
      item,
      {withCredentials: true});
  }

  delete(id: string) {
    return this.http.delete(Server.api(`editor/pages/${id}`), {
      withCredentials: true,
      responseType: 'text'
    });
  }

}
