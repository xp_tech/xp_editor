import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DefGetItemsParams, Server} from '../../utils/router.utils';
import {ChunkModel} from '../models/chunks.model';

@Injectable({
  providedIn: 'root'
})
export class ChunksService {

  constructor(private http: HttpClient) { }

  getAll(params: DefGetItemsParams) {
    const options = {
      params: {
        limit: params.limit || '20',
        offset: params.offset || '0',
        order: params.order || 'updatedAt',
        desc: params.desc || 'true',
      },
      withCredentials: true
    };
    if (params.filterID) (options.params as any).filterID = params.filterID;
    if (params.filterTag) (options.params as any).filterTag = JSON.stringify(params.filterTag);

    return this.http.get(Server.api('editor/chunks'), options);
  }

  get(id: string) {
    return this.http.get(Server.api(`editor/chunks/${id}`), {withCredentials: true});
  }

  add(item: ChunkModel) {
    return this.http.post(Server.api(`editor/chunks/${item.id}`),
      {
        body: item.body,
        description: item.description,
        tags: item.tags
      },
      {withCredentials: true});
  }

  update(item: ChunkModel) {
    return this.http.put(Server.api(`editor/chunks/${item.id}`),
      {
        body: item.body,
        description: item.description,
        tags: item.tags
      },
      {withCredentials: true});
  }

  delete(id: string) {
    return this.http.delete(Server.api(`editor/chunks/${id}`), {
      withCredentials: true,
      responseType: 'text'
    });
  }

}
