import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DefGetItemsParams, Server} from '../../utils/router.utils';


@Injectable({
  providedIn: 'root'
})
export class TagsService {


  constructor(private http: HttpClient) {
  }

  getAll(entity: string, params: DefGetItemsParams) {
    const options = {
      params: {
        limit: params.limit || '100',
        offset: params.offset || '0',
        order: params.order || 'id',
        desc: params.desc || 'true',
      },
      withCredentials: true
    };
    if (params.filterID) { (options.params as any).filterID =  params.filterID; }

    return this.http.get(Server.api(`editor/${entity}/tags`), options);

  }

  get(entity: string, id: string, description: string) {
    const options = {
      params: {},
      withCredentials: true
    };
    if (description) { (options.params as any).description =  description; }
    return this.http.get(Server.api(`editor/${entity}/tags/${id}`), options);

  }

  delete(entity: string, id: string) {
    return this.http.delete(Server.api(`editor/${entity}/tags/${id}`), {
      withCredentials: true,
      responseType: 'text'
    });
  }
}
