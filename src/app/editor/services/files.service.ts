import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Server} from '../../utils/router.utils';

@Injectable({
  providedIn: 'root'
})
export class FilesService {

  constructor(private http: HttpClient) { }

  loadDir(path: string) {
    const p = (path) ? `files/dir?url=${path}`: `files/dir`;
    return this.http.get(Server.api(p), {withCredentials: true});
  }

  createDir(path: string) {
    return this.http.post(Server.api(`files/dir`), {url: path}, {
      withCredentials: true,
      responseType: 'text'
    });
  }

  deleteDir(path: string) {
    return this.http.delete(Server.api(`files/dir?url=${path}`), {
      withCredentials: true,
      responseType: 'text'
    });
  }

  uploadFile(data: FormData) {
    return this.http.post(Server.api(`files/upload`), data, {
      withCredentials: true,
      responseType: 'text'
    });
  }

  deleteFile(path: string) {
    return this.http.delete(Server.api(`files/file?url=${path}`), {
      withCredentials: true,
      responseType: 'text'
    });
  }
}
