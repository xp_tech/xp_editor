import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DefGetItemsParams, Server} from '../../utils/router.utils';
import {ArticleModel} from '../models/articles.model';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  constructor(private http: HttpClient) { }

  getAll(params: DefGetItemsParams) {
    const options = {
      params: {
        limit: params.limit || '20',
        offset: params.offset || '0',
        order: params.order || 'updatedAt',
        desc: params.desc || 'true',
      },
      withCredentials: true
    };

    for (const i of ['filterID', 'filterURL', 'filterHeader', 'filterSummary', 'filterLocale', 'filterSubdomain', 'filterPublished']) {
      if (params[i]) (options.params as any)[i] = params[i];
    }
    if (params.filterTag) (options.params as any).filterTag = JSON.stringify(params.filterTag);

    return this.http.get(Server.api('editor/articles'), options);
  }

  get(id: string) {
    return this.http.get(Server.api(`editor/articles/${id}`), {withCredentials: true});
  }

  add(item: ArticleModel) {
    return this.http.post(Server.api(`editor/articles/${item.id}`),
      item,
      {withCredentials: true});
  }

  update(item: ArticleModel) {
    return this.http.put(Server.api(`editor/articles/${item.id}`),
      item,
      {withCredentials: true});
  }

  delete(id: string) {
    return this.http.delete(Server.api(`editor/articles/${id}`), {
      withCredentials: true,
      responseType: 'text'
    });
  }

}
