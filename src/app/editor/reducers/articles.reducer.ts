import {ArticleModel} from '../models/articles.model';
import {ArticlesActions, ArticlesActionTypes} from '../actions/articles.actions';
import {createFeatureSelector, createSelector} from '@ngrx/store';

export interface State {
  error?: string;
  count?: number;
  rows?: ArticleModel[];
  active?: ArticleModel;
}

export const initialState: State = {
  count: 0,
  rows: [],
  active: {
    id: undefined,
    description: undefined,
    body: undefined,
    tags: [],
    url: undefined, // maxLength: 128
    title: undefined, // maxLength: 255
    keywords:	undefined, // maxLength: 255
    template: undefined,
    header: undefined,
    summary: undefined,
    ogImage: {
      url: undefined, // maxLength: 255
      width: undefined, // >= 0
      height: undefined, // >= 0
    },
    locale: undefined, // from settings
    subdomain: undefined, // from settings
    published: false,
  }
};

export function  reducer(state = initialState, action: ArticlesActions): State {
  switch (action.type) {

    case ArticlesActionTypes.ResetArticle: {
      return state;
    }

    case ArticlesActionTypes.ResetArticleSuccess: {
      return {
        ...state,
        active: {
          id: undefined,
          description: undefined,
          body: undefined,
          tags: [],
          url: undefined, // maxLength: 128
          title: undefined, // maxLength: 255
          keywords:	undefined, // maxLength: 255
          template: undefined,
          header: undefined,
          summary: undefined,
          ogImage: {
            url: undefined, // maxLength: 255
            width: undefined, // >= 0
            height: undefined, // >= 0
          },
          locale: undefined, // from settings
          subdomain: undefined, // from settings
          published: false,
        }
      };
    }

    case ArticlesActionTypes.LoadArticles: {
      return state;
    }

    case ArticlesActionTypes.LoadArticlesSuccess: {
      return {
        count: action.payload.count,
        rows: action.payload.rows,
        active: {
          id: undefined,
          description: undefined,
          body: undefined,
          tags: [],
          url: undefined, // maxLength: 128
          title: undefined, // maxLength: 255
          keywords:	undefined, // maxLength: 255
          template: undefined,
          header: undefined,
          summary: undefined,
          ogImage: {
            url: undefined, // maxLength: 255
            width: undefined, // >= 0
            height: undefined, // >= 0
          },
          locale: undefined, // from settings
          subdomain: undefined, // from settings
          published: false,
        }
      };
    }

    case ArticlesActionTypes.LoadArticlesFailure: {
      return {
        ...state,
        error: action.payload.message
      };
    }

    case ArticlesActionTypes.GetArticle: {
      return state;
    }

    case ArticlesActionTypes.GetArticleSuccess: {
      return {
        ...state,
        active: {
          id: action.payload.id,
          description: action.payload.description,
          body: action.payload.body,
          tags: action.payload.tags,
          url: action.payload.url, // maxLength: 128
          title: action.payload.title, // maxLength: 255
          keywords:	action.payload.keywords, // maxLength: 255
          template: action.payload.template,
          header: action.payload.header,
          summary: action.payload.summary,
          ogImage: {
            url: action.payload.ogImage.url, // maxLength: 255
            width: action.payload.ogImage.width, // >= 0
            height: action.payload.ogImage.height, // >= 0
          },
          locale: action.payload.locale, // from settings
          subdomain: action.payload.subdomain, // from settings
          published: action.payload.published,
        }
      };
    }

    case ArticlesActionTypes.GetArticleFailure: {
      return {
        ...state,
        error: action.payload.message
      };
    }

    case ArticlesActionTypes.CreateArticle: {
      return state;
    }

    case ArticlesActionTypes.CreateArticleSuccess: {
      return {
        ...state,
        active: action.payload
      };
    }

    case ArticlesActionTypes.CreateArticleFailure: {
      return {
        ...state,
        error: action.payload.message
      };
    }

    case ArticlesActionTypes.UpdateArticle: {
      return state;
    }

    case ArticlesActionTypes.UpdateArticleSuccess: {
      return {
        ...state,
        active: action.payload
      };
    }

    case ArticlesActionTypes.UpdateArticleFailure: {
      return {
        ...state,
        error: action.payload.message
      };
    }

    case ArticlesActionTypes.DeleteArticle: {
      return state;
    }

    case ArticlesActionTypes.DeleteArticleSuccess: {
      return {
        ...state,
        active: {
          id: undefined,
          description: undefined,
          body: undefined,
          tags: [],
          url: undefined, // maxLength: 128
          title: undefined, // maxLength: 255
          keywords:	undefined, // maxLength: 255
          template: undefined,
          header: undefined,
          summary: undefined,
          ogImage: {
            url: undefined, // maxLength: 255
            width: undefined, // >= 0
            height: undefined, // >= 0
          },
          locale: undefined, // from settings
          subdomain: undefined, // from settings
          published: false,
        }
      };
    }

    case ArticlesActionTypes.DeleteArticleFailure: {
      return {
        ...state,
        error: action.payload.message
      };
    }

    default:
      return state;
  }
}

export const selectFeatureState = createFeatureSelector<State>('articlesState');
export const getCount = createSelector(
  selectFeatureState,
  (state: State) => state.count
);
export const getRows = createSelector(
  selectFeatureState,
  (state: State) => state.rows
);
export const getActive = createSelector(
  selectFeatureState,
  (state: State) => state.active
);
export const getError = createSelector(
  selectFeatureState,
  (state: State) => state.error
);


