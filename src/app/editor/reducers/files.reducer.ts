import {FileModel} from '../models/files.model';
import {FilesActions, FilesActionTypes} from '../actions/files.actions';
import {createFeatureSelector, createSelector} from '@ngrx/store';

export interface State {
  error?: string;
  path?: string;
  list?: FileModel[];
}

export const initialState: State = {
  list: [],
  path: ''
};

export function reducer(state = initialState, action: FilesActions): State {
  switch (action.type) {

    case FilesActionTypes.CreateDir: {
      return state;
    }

    case FilesActionTypes.CreateDirFailure: {
      return {
        ...state,
        error: action.payload.message
      };
    }

    case FilesActionTypes.CreateDirSuccess: {
      return state;
    }

    case FilesActionTypes.DeleteDir: {
      return state;
    }

    case FilesActionTypes.DeleteDirFailure: {
      return {
        ...state,
        error: action.payload.message
      };    }

    case FilesActionTypes.DeleteDirSuccess: {
      return state;
    }

    case FilesActionTypes.DeleteFile: {
      return state;
    }

    case FilesActionTypes.DeleteFileFailure: {
      return {
        ...state,
        error: action.payload.message
      };    }

    case FilesActionTypes.DeleteFileSuccess: {
      return state;
    }

    case FilesActionTypes.LoadFiles: {
      return state;
    }

    case FilesActionTypes.LoadFilesFailure: {
      return {
        ...state,
        error: action.payload.message
      };    }

    case FilesActionTypes.LoadFilesSuccess: {
      return {
        ...state,
        path: action.payload.path,
        list: action.payload.list
      };
    }

    case FilesActionTypes.UploadFile: {
      return state;
    }

    case FilesActionTypes.UploadFileFailure: {
      return {
        ...state,
        error: action.payload.message
      };    }

    case FilesActionTypes.UploadFileSuccess: {
      return state;
    }

    default:
      return state;
  }
}

export const selectFeatureState = createFeatureSelector<State>('filesState');
export const getList = createSelector(
  selectFeatureState,
  (state: State) => state.list
);
export const getPath = createSelector(
  selectFeatureState,
  (state: State) => state.path
);
export const getError = createSelector(
  selectFeatureState,
  (state: State) => state.error
);
