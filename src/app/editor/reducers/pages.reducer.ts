import {PageModel} from '../models/pages.model';
import {PagesActions, PagesActionTypes} from '../actions/pages.actions';
import {createFeatureSelector, createSelector} from '@ngrx/store';

export interface State {
  error?: string;
  count?: number;
  rows?: PageModel[];
  active?: PageModel;
}

export const initialState: State = {
  count: 0,
  rows: [],
  active: {
    id: undefined,
    description: undefined,
    body: undefined,
    tags: [],
    url: undefined, // maxLength: 128
    title: undefined, // maxLength: 255
    keywords:	undefined, // maxLength: 255
    css: undefined,
    ogImage: {
      url: undefined, // maxLength: 255
      width: undefined, // >= 0
      height: undefined, // >= 0
    },
    locale: undefined, // from settings
    subdomain: undefined, // from settings
    published: false,
  }
};

export function  reducer(state = initialState, action: PagesActions): State {
  switch (action.type) {

    case PagesActionTypes.ResetPage: {
      return state;
    }

    case PagesActionTypes.ResetPageSuccess: {
      return {
        ...state,
        active: {
          id: undefined,
          description: undefined,
          body: undefined,
          tags: [],
          url: undefined, // maxLength: 128
          title: undefined, // maxLength: 255
          keywords:	undefined, // maxLength: 255
          css: undefined,
          ogImage: {
            url: undefined, // maxLength: 255
            width: undefined, // >= 0
            height: undefined, // >= 0
          },
          locale: undefined, // from settings
          subdomain: undefined, // from settings
          published: false,
        }
      };
    }

    case PagesActionTypes.LoadPages: {
      return state;
    }

    case PagesActionTypes.LoadPagesSuccess: {
      return {
        count: action.payload.count,
        rows: action.payload.rows,
        active: {
          id: undefined,
          description: undefined,
          body: undefined,
          tags: [],
          url: undefined, // maxLength: 128
          title: undefined, // maxLength: 255
          keywords:	undefined, // maxLength: 255
          css: undefined,
          ogImage: {
            url: undefined, // maxLength: 255
            width: undefined, // >= 0
            height: undefined, // >= 0
          },
          locale: undefined, // from settings
          subdomain: undefined, // from settings
          published: false,
        }
      };
    }

    case PagesActionTypes.LoadPagesFailure: {
      return {
        ...state,
        error: action.payload.message
      };
    }

    case PagesActionTypes.GetPage: {
      return state;
    }

    case PagesActionTypes.GetPageSuccess: {
      return {
        ...state,
        active: {
          id: action.payload.id,
          description: action.payload.description,
          body: action.payload.body,
          tags: action.payload.tags,
          url: action.payload.url, // maxLength: 128
          title: action.payload.title, // maxLength: 255
          keywords:	action.payload.keywords, // maxLength: 255
          css: action.payload.css,
          ogImage: {
            url: action.payload.ogImage.url, // maxLength: 255
            width: action.payload.ogImage.width, // >= 0
            height: action.payload.ogImage.height, // >= 0
          },
          locale: action.payload.locale, // from settings
          subdomain: action.payload.subdomain, // from settings
          published: action.payload.published,
        }
      };
    }

    case PagesActionTypes.GetPageFailure: {
      return {
        ...state,
        error: action.payload.message
      };
    }

    case PagesActionTypes.CreatePage: {
      return state;
    }

    case PagesActionTypes.CreatePageSuccess: {
      return {
        ...state,
        active: action.payload
      };
    }

    case PagesActionTypes.CreatePageFailure: {
      return {
        ...state,
        error: action.payload.message
      };
    }

    case PagesActionTypes.UpdatePage: {
      return state;
    }

    case PagesActionTypes.UpdatePageSuccess: {
      return {
        ...state,
        active: action.payload
      };
    }

    case PagesActionTypes.UpdatePageFailure: {
      return {
        ...state,
        error: action.payload.message
      };
    }

    case PagesActionTypes.DeletePage: {
      return state;
    }

    case PagesActionTypes.DeletePageSuccess: {
      return {
        ...state,
        active: {
          id: undefined,
          description: undefined,
          body: undefined,
          tags: [],
          url: undefined, // maxLength: 128
          title: undefined, // maxLength: 255
          keywords:	undefined, // maxLength: 255
          css: undefined,
          ogImage: {
            url: undefined, // maxLength: 255
            width: undefined, // >= 0
            height: undefined, // >= 0
          },
          locale: undefined, // from settings
          subdomain: undefined, // from settings
          published: false,
        }
      };
    }

    case PagesActionTypes.DeletePageFailure: {
      return {
        ...state,
        error: action.payload.message
      };
    }

    default:
      return state;
  }
}

export const selectFeatureState = createFeatureSelector<State>('pagesState');
export const getCount = createSelector(
  selectFeatureState,
  (state: State) => state.count
);
export const getRows = createSelector(
  selectFeatureState,
  (state: State) => state.rows
);
export const getActive = createSelector(
  selectFeatureState,
  (state: State) => state.active
);
export const getError = createSelector(
  selectFeatureState,
  (state: State) => state.error
);


