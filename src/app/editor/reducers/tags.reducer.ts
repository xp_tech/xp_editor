import {createFeatureSelector, createSelector} from '@ngrx/store';
import {TagModel} from '../models/tag.model';
import {TagsActions, TagsActionTypes} from '../actions/tags.actions';

export interface State {
  error?: string;
  count?: number;
  rows?: TagModel[];
  selectedBrowser: string[];
  selectedEditor: string[];
}

export const initialState: State = {
  count: 0,
  rows: [],
  selectedBrowser: undefined,
  selectedEditor: []
};

export function reducer(state = initialState, action: TagsActions): State {
  switch (action.type) {

    case TagsActionTypes.LoadTags: {
      return state;
    }

    case TagsActionTypes.LoadTagsSuccess: {
      return  {
        ...state,
        count: action.payload.count,
        rows: action.payload.rows
      };
    }

    case TagsActionTypes.LoadTagsFailure: {
      return {
        ...state,
        error: action.payload.message
      };
    }

    case TagsActionTypes.GetTag: {
      return state;
    }

    case TagsActionTypes.GetTagSuccess: {
      return state;
    }

    case TagsActionTypes.GetTagFailure: {
      return {
        ...state,
        error: action.payload.message
      };
    }

    case TagsActionTypes.DeleteTag: {
      return state;
    }

    case TagsActionTypes.DeleteTagSuccess: {
      return state;
    }

    case TagsActionTypes.DeleteTagFailure: {
      return {
        ...state,
        error: action.payload.message
      };
    }

    case TagsActionTypes.SetSelected: {
      return state;
    }

    case TagsActionTypes.SetSelectedBrowserSuccess: {
      return {
        ...state,
        selectedBrowser: action.payload
      };
    }

    case TagsActionTypes.SetSelectedEditorSuccess: {
      return {
        ...state,
        selectedEditor: action.payload
      };
    }

    default:
      return state;
  }
}

export const selectFeatureState = createFeatureSelector<State>('tagsState');
export const getCount = createSelector(
  selectFeatureState,
  (state: State) => state.count
);
export const getRows = createSelector(
  selectFeatureState,
  (state: State) => state.rows
);
export const getSelectedBrowser = createSelector(
  selectFeatureState,
  (state: State) => state.selectedBrowser
);
export const getSelectedEditor = createSelector(
  selectFeatureState,
  (state: State) => state.selectedEditor
);
export const getError = createSelector(
  selectFeatureState,
  (state: State) => state.error
);
