import {ChunkModel} from '../models/chunks.model';
import {ChunksActions, ChunksActionTypes} from '../actions/chunks.actions';
import {createFeatureSelector, createSelector} from '@ngrx/store';

export interface State {
  error?: string;
  count?: number;
  rows?: ChunkModel[];
  active?: ChunkModel;
}

export const initialState: State = {
  count: 0,
  rows: [],
  active: {
    id: undefined,
    description: undefined,
    body: undefined,
    tags: []
  }
};

export function  reducer(state = initialState, action: ChunksActions): State {
  switch (action.type) {

    case ChunksActionTypes.ResetChunk: {
      return state;
    }

    case ChunksActionTypes.ResetChunkSuccess: {
      return {
        ...state,
        active: {
          id: undefined,
          description: undefined,
          body: undefined,
          tags: []
        }
      };
    }

    case ChunksActionTypes.LoadChunks: {
      return state;
    }

    case ChunksActionTypes.LoadChunksSuccess: {
      return {
        count: action.payload.count,
        rows: action.payload.rows,
        active: {
          id: undefined,
          description: undefined,
          body: undefined,
          tags: []
        }
      };
    }

    case ChunksActionTypes.LoadChunksFailure: {
      return {
        ...state,
        error: action.payload.message
      };
    }

    case ChunksActionTypes.GetChunk: {
      return state;
    }

    case ChunksActionTypes.GetChunkSuccess: {
      return {
        ...state,
        active: {
          id: action.payload.id,
          description: action.payload.description,
          body: action.payload.body,
          tags: action.payload.tags
        }
      };
    }

    case ChunksActionTypes.GetChunkFailure: {
      return {
        ...state,
        error: action.payload.message
      };
    }

    case ChunksActionTypes.CreateChunk: {
      return state;
    }

    case ChunksActionTypes.CreateChunkSuccess: {
      return {
        ...state,
        active: action.payload
      };
    }

    case ChunksActionTypes.CreateChunkFailure: {
      return {
        ...state,
        error: action.payload.message
      };
    }

    case ChunksActionTypes.UpdateChunk: {
      return state;
    }

    case ChunksActionTypes.UpdateChunkSuccess: {
      return {
        ...state,
        active: action.payload
      };
    }

    case ChunksActionTypes.UpdateChunkFailure: {
      return {
        ...state,
        error: action.payload.message
      };
    }

    case ChunksActionTypes.DeleteChunk: {
      return state;
    }

    case ChunksActionTypes.DeleteChunkSuccess: {
      return {
        ...state,
        active: {
          id: undefined,
          description: undefined,
          body: undefined,
          tags: []
        }
      };
    }

    case ChunksActionTypes.DeleteChunkFailure: {
      return {
        ...state,
        error: action.payload.message
      };
    }

    default:
      return state;
  }
}

export const selectFeatureState = createFeatureSelector<State>('chunksState');
export const getCount = createSelector(
  selectFeatureState,
  (state: State) => state.count
);
export const getRows = createSelector(
  selectFeatureState,
  (state: State) => state.rows
);
export const getActive = createSelector(
  selectFeatureState,
  (state: State) => state.active
);
export const getError = createSelector(
  selectFeatureState,
  (state: State) => state.error
);


