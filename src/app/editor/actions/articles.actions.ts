import { Action } from '@ngrx/store';
import {DefGetItemsParams} from '../../utils/router.utils';
import {ArticleModel, LoadedArticlesModel} from '../models/articles.model';

export enum ArticlesActionTypes {
  LoadArticles = '[Articles] Load Articles',
  LoadArticlesSuccess = '[Articles] Load Articles Success',
  LoadArticlesFailure = '[Articles] Load Articles Failure',

  GetArticle = '[Articles] Get Article',
  GetArticleSuccess = '[Articles] Get Article Success',
  GetArticleFailure = '[Articles] Get Article Failure',

  CreateArticle = '[Articles] Create Article',
  CreateArticleSuccess = '[Articles] Create Article Success',
  CreateArticleFailure = '[Articles] Create Article Failure',

  UpdateArticle = '[Articles] Update Article',
  UpdateArticleSuccess = '[Articles] Update Article Success',
  UpdateArticleFailure = '[Articles] Update Article Failure',

  DeleteArticle = '[Articles] Delete Article',
  DeleteArticleSuccess = '[Articles] Delete Article Success',
  DeleteArticleFailure = '[Articles] Delete Article Failure',

  ResetArticle = '[Articles] Reset Article',
  ResetArticleSuccess = '[Articles] Reset Article Success',
}


export class LoadArticles implements Action {
  readonly type = ArticlesActionTypes.LoadArticles;
  constructor(public payload: DefGetItemsParams) {}
}

export class LoadArticlesSuccess implements Action {
  readonly type = ArticlesActionTypes.LoadArticlesSuccess;
  constructor(public payload: LoadedArticlesModel) {}
}

export class LoadArticlesFailure implements Action {
  readonly type = ArticlesActionTypes.LoadArticlesFailure;
  constructor(public payload: any) {}
}

export class GetArticle implements Action {
  readonly type = ArticlesActionTypes.GetArticle;
  constructor(public payload: {id: string, copy?: boolean}) {}
}

export class GetArticleSuccess implements Action {
  readonly type = ArticlesActionTypes.GetArticleSuccess;
  constructor(public payload: ArticleModel) {}
}

export class GetArticleFailure implements Action {
  readonly type = ArticlesActionTypes.GetArticleFailure;
  constructor(public payload: any) {}
}

export class CreateArticle implements Action {
  readonly type = ArticlesActionTypes.CreateArticle;
  constructor(public payload: ArticleModel) {}
}

export class CreateArticleSuccess implements Action {
  readonly type = ArticlesActionTypes.CreateArticleSuccess;
  constructor(public payload: ArticleModel) {}
}

export class CreateArticleFailure implements Action {
  readonly type = ArticlesActionTypes.CreateArticleFailure;
  constructor(public payload: any) {}
}

export class UpdateArticle implements Action {
  readonly type = ArticlesActionTypes.UpdateArticle;
  constructor(public payload: ArticleModel) {}
}

export class UpdateArticleSuccess implements Action {
  readonly type = ArticlesActionTypes.UpdateArticleSuccess;
  constructor(public payload: ArticleModel) {}
}

export class UpdateArticleFailure implements Action {
  readonly type = ArticlesActionTypes.UpdateArticleFailure;
  constructor(public payload: any) {}
}

export class DeleteArticle implements Action {
  readonly type = ArticlesActionTypes.DeleteArticle;
  constructor(public payload: string) {}
}

export class DeleteArticleSuccess implements Action {
  readonly type = ArticlesActionTypes.DeleteArticleSuccess;
}

export class DeleteArticleFailure implements Action {
  readonly type = ArticlesActionTypes.DeleteArticleFailure;
  constructor(public payload: any) {}
}

export class ResetArticle implements Action {
  readonly type = ArticlesActionTypes.ResetArticle;
}

export class ResetArticleSuccess implements Action {
  readonly type = ArticlesActionTypes.ResetArticleSuccess;
}

export type ArticlesActions =
  | LoadArticles
  | LoadArticlesSuccess
  | LoadArticlesFailure
  | GetArticle
  | GetArticleSuccess
  | GetArticleFailure
  | CreateArticle
  | CreateArticleSuccess
  | CreateArticleFailure
  | UpdateArticle
  | UpdateArticleSuccess
  | UpdateArticleFailure
  | DeleteArticle
  | DeleteArticleSuccess
  | DeleteArticleFailure
  | ResetArticle
  | ResetArticleSuccess;
