import { Action } from '@ngrx/store';
import {DefGetItemsParams} from '../../utils/router.utils';
import {ChunkModel, LoadedChunksModel} from '../models/chunks.model';

export enum ChunksActionTypes {
  LoadChunks = '[Chunks] Load Chunks',
  LoadChunksSuccess = '[Chunks] Load Chunks Success',
  LoadChunksFailure = '[Chunks] Load Chunks Failure',

  GetChunk = '[Chunks] Get Chunk',
  GetChunkSuccess = '[Chunks] Get Chunk Success',
  GetChunkFailure = '[Chunks] Get Chunk Failure',

  CreateChunk = '[Chunks] Create Chunk',
  CreateChunkSuccess = '[Chunks] Create Chunk Success',
  CreateChunkFailure = '[Chunks] Create Chunk Failure',

  UpdateChunk = '[Chunks] Update Chunk',
  UpdateChunkSuccess = '[Chunks] Update Chunk Success',
  UpdateChunkFailure = '[Chunks] Update Chunk Failure',

  DeleteChunk = '[Chunks] Delete Chunk',
  DeleteChunkSuccess = '[Chunks] Delete Chunk Success',
  DeleteChunkFailure = '[Chunks] Delete Chunk Failure',

  ResetChunk = '[Chunks] Reset Chunk',
  ResetChunkSuccess = '[Chunks] Reset Chunk Success',
}


export class LoadChunks implements Action {
  readonly type = ChunksActionTypes.LoadChunks;
  constructor(public payload: DefGetItemsParams) {}
}

export class LoadChunksSuccess implements Action {
  readonly type = ChunksActionTypes.LoadChunksSuccess;
  constructor(public payload: LoadedChunksModel) {}
}

export class LoadChunksFailure implements Action {
  readonly type = ChunksActionTypes.LoadChunksFailure;
  constructor(public payload: any) {}
}

export class GetChunk implements Action {
  readonly type = ChunksActionTypes.GetChunk;
  constructor(public payload: {id: string, copy?: boolean}) {}
}

export class GetChunkSuccess implements Action {
  readonly type = ChunksActionTypes.GetChunkSuccess;
  constructor(public payload: ChunkModel) {}
}

export class GetChunkFailure implements Action {
  readonly type = ChunksActionTypes.GetChunkFailure;
  constructor(public payload: any) {}
}

export class CreateChunk implements Action {
  readonly type = ChunksActionTypes.CreateChunk;
  constructor(public payload: ChunkModel) {}
}

export class CreateChunkSuccess implements Action {
  readonly type = ChunksActionTypes.CreateChunkSuccess;
  constructor(public payload: ChunkModel) {}
}

export class CreateChunkFailure implements Action {
  readonly type = ChunksActionTypes.CreateChunkFailure;
  constructor(public payload: any) {}
}

export class UpdateChunk implements Action {
  readonly type = ChunksActionTypes.UpdateChunk;
  constructor(public payload: ChunkModel) {}
}

export class UpdateChunkSuccess implements Action {
  readonly type = ChunksActionTypes.UpdateChunkSuccess;
  constructor(public payload: ChunkModel) {}
}

export class UpdateChunkFailure implements Action {
  readonly type = ChunksActionTypes.UpdateChunkFailure;
  constructor(public payload: any) {}
}

export class DeleteChunk implements Action {
  readonly type = ChunksActionTypes.DeleteChunk;
  constructor(public payload: string) {}
}

export class DeleteChunkSuccess implements Action {
  readonly type = ChunksActionTypes.DeleteChunkSuccess;
}

export class DeleteChunkFailure implements Action {
  readonly type = ChunksActionTypes.DeleteChunkFailure;
  constructor(public payload: any) {}
}

export class ResetChunk implements Action {
  readonly type = ChunksActionTypes.ResetChunk;
}

export class ResetChunkSuccess implements Action {
  readonly type = ChunksActionTypes.ResetChunkSuccess;
}

export type ChunksActions =
  | LoadChunks
  | LoadChunksSuccess
  | LoadChunksFailure
  | GetChunk
  | GetChunkSuccess
  | GetChunkFailure
  | CreateChunk
  | CreateChunkSuccess
  | CreateChunkFailure
  | UpdateChunk
  | UpdateChunkSuccess
  | UpdateChunkFailure
  | DeleteChunk
  | DeleteChunkSuccess
  | DeleteChunkFailure
  | ResetChunk
  | ResetChunkSuccess;
