import { Action } from '@ngrx/store';
import {FileModel} from '../models/files.model';

export enum FilesActionTypes {
  LoadFiles         = '[Files] Load Files',
  LoadFilesSuccess  = '[Files] Load Files Success',
  LoadFilesFailure  = '[Files] Load Files Failure',

  CreateDir         = '[Files] Create Directory',
  CreateDirSuccess  = '[Files] Create Directory Success',
  CreateDirFailure  = '[Files] Create Directory Failure',

  DeleteDir         = '[Files] Delete Directory',
  DeleteDirSuccess  = '[Files] Delete Directory Success',
  DeleteDirFailure  = '[Files] Delete Directory Failure',

  DeleteFile        = '[Files] Delete File',
  DeleteFileSuccess = '[Files] Delete File Success',
  DeleteFileFailure = '[Files] Delete File Failure',

  UploadFile        = '[Files] Upload File',
  UploadFileSuccess = '[Files] Upload File Success',
  UploadFileFailure = '[Files] Upload File Failure',
}

export class LoadFiles implements Action {
  readonly type = FilesActionTypes.LoadFiles;
  constructor(public payload: string) {}
}

export class LoadFilesSuccess implements Action {
  readonly type = FilesActionTypes.LoadFilesSuccess;
  constructor(public payload: {list: FileModel[], path: string}) {}
}

export class LoadFilesFailure implements Action {
  readonly type = FilesActionTypes.LoadFilesFailure;
  constructor(public payload: any) {}
}

export class CreateDir implements Action {
  readonly type = FilesActionTypes.CreateDir;
  constructor(public payload: {parent: string, path: string}) {}
}

export class CreateDirSuccess implements Action {
  readonly type = FilesActionTypes.CreateDirSuccess;
}

export class CreateDirFailure implements Action {
  readonly type = FilesActionTypes.CreateDirFailure;
  constructor(public payload: any) {}

}

export class DeleteDir implements Action {
  readonly type = FilesActionTypes.DeleteDir;
  constructor(public payload: {parent: string, path: string}) {}
}

export class DeleteDirSuccess implements Action {
  readonly type = FilesActionTypes.DeleteDirSuccess;
}

export class DeleteDirFailure implements Action {
  readonly type = FilesActionTypes.DeleteDirFailure;
  constructor(public payload: any) {}
}

export class DeleteFile implements Action {
  readonly type = FilesActionTypes.DeleteFile;
  constructor(public payload: {parent: string, path: string}) {}
}

export class DeleteFileSuccess implements Action {
  readonly type = FilesActionTypes.DeleteFileSuccess;
}

export class DeleteFileFailure implements Action {
  readonly type = FilesActionTypes.DeleteFileFailure;
  constructor(public payload: any) {}
}

export class UploadFile implements Action {
  readonly type = FilesActionTypes.UploadFile;
  constructor(public payload: {parent: string, data: FormData}) {}
}

export class UploadFileSuccess implements Action {
  readonly type = FilesActionTypes.UploadFileSuccess;
}

export class UploadFileFailure implements Action {
  readonly type = FilesActionTypes.UploadFileFailure;
  constructor(public payload: any) {}
}


export type FilesActions =
  | LoadFiles
  | LoadFilesSuccess
  | LoadFilesFailure
  | CreateDir
  | CreateDirSuccess
  | CreateDirFailure
  | DeleteDir
  | DeleteDirSuccess
  | DeleteDirFailure
  | DeleteFile
  | DeleteFileSuccess
  | DeleteFileFailure
  | UploadFile
  | UploadFileSuccess
  | UploadFileFailure;
