import { Action } from '@ngrx/store';
import {DefGetItemsParams} from '../../utils/router.utils';
import {PageModel, LoadedPagesModel} from '../models/pages.model';

export enum PagesActionTypes {
  LoadPages = '[Pages] Load Pages',
  LoadPagesSuccess = '[Pages] Load Pages Success',
  LoadPagesFailure = '[Pages] Load Pages Failure',

  GetPage = '[Pages] Get Page',
  GetPageSuccess = '[Pages] Get Page Success',
  GetPageFailure = '[Pages] Get Page Failure',

  CreatePage = '[Pages] Create Page',
  CreatePageSuccess = '[Pages] Create Page Success',
  CreatePageFailure = '[Pages] Create Page Failure',

  UpdatePage = '[Pages] Update Page',
  UpdatePageSuccess = '[Pages] Update Page Success',
  UpdatePageFailure = '[Pages] Update Page Failure',

  DeletePage = '[Pages] Delete Page',
  DeletePageSuccess = '[Pages] Delete Page Success',
  DeletePageFailure = '[Pages] Delete Page Failure',

  ResetPage = '[Pages] Reset Page',
  ResetPageSuccess = '[Pages] Reset Page Success',
}


export class LoadPages implements Action {
  readonly type = PagesActionTypes.LoadPages;
  constructor(public payload: DefGetItemsParams) {}
}

export class LoadPagesSuccess implements Action {
  readonly type = PagesActionTypes.LoadPagesSuccess;
  constructor(public payload: LoadedPagesModel) {}
}

export class LoadPagesFailure implements Action {
  readonly type = PagesActionTypes.LoadPagesFailure;
  constructor(public payload: any) {}
}

export class GetPage implements Action {
  readonly type = PagesActionTypes.GetPage;
  constructor(public payload: {id: string, copy?: boolean}) {}
}

export class GetPageSuccess implements Action {
  readonly type = PagesActionTypes.GetPageSuccess;
  constructor(public payload: PageModel) {}
}

export class GetPageFailure implements Action {
  readonly type = PagesActionTypes.GetPageFailure;
  constructor(public payload: any) {}
}

export class CreatePage implements Action {
  readonly type = PagesActionTypes.CreatePage;
  constructor(public payload: PageModel) {}
}

export class CreatePageSuccess implements Action {
  readonly type = PagesActionTypes.CreatePageSuccess;
  constructor(public payload: PageModel) {}
}

export class CreatePageFailure implements Action {
  readonly type = PagesActionTypes.CreatePageFailure;
  constructor(public payload: any) {}
}

export class UpdatePage implements Action {
  readonly type = PagesActionTypes.UpdatePage;
  constructor(public payload: PageModel) {}
}

export class UpdatePageSuccess implements Action {
  readonly type = PagesActionTypes.UpdatePageSuccess;
  constructor(public payload: PageModel) {}
}

export class UpdatePageFailure implements Action {
  readonly type = PagesActionTypes.UpdatePageFailure;
  constructor(public payload: any) {}
}

export class DeletePage implements Action {
  readonly type = PagesActionTypes.DeletePage;
  constructor(public payload: string) {}
}

export class DeletePageSuccess implements Action {
  readonly type = PagesActionTypes.DeletePageSuccess;
}

export class DeletePageFailure implements Action {
  readonly type = PagesActionTypes.DeletePageFailure;
  constructor(public payload: any) {}
}

export class ResetPage implements Action {
  readonly type = PagesActionTypes.ResetPage;
}

export class ResetPageSuccess implements Action {
  readonly type = PagesActionTypes.ResetPageSuccess;
}

export type PagesActions =
  | LoadPages
  | LoadPagesSuccess
  | LoadPagesFailure
  | GetPage
  | GetPageSuccess
  | GetPageFailure
  | CreatePage
  | CreatePageSuccess
  | CreatePageFailure
  | UpdatePage
  | UpdatePageSuccess
  | UpdatePageFailure
  | DeletePage
  | DeletePageSuccess
  | DeletePageFailure
  | ResetPage
  | ResetPageSuccess;
