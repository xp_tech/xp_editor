import { Action } from '@ngrx/store';
import {LoadedTagsModel, TagsGetParams} from '../models/tag.model';

export enum TagsActionTypes {
  LoadTags = '[Tags] Load Tags',
  LoadTagsSuccess = '[Tags] Load Tags Success',
  LoadTagsFailure = '[Tags] Load Tags Failure',

  GetTag = '[Tags] Get Tag',
  GetTagSuccess = '[Tags] Get Tag Success',
  GetTagFailure = '[Tags] Get Tag Failure',

  DeleteTag = '[Tags] Delete Tag',
  DeleteTagSuccess = '[Tags] Delete Tag Success',
  DeleteTagFailure = '[Tags] Delete Tag Failure',

  SetSelected = '[Tags] Set Selected',
  SetSelectedBrowserSuccess = '[Tags] Set Selected Browser Success',
  SetSelectedEditorSuccess = '[Tags] Set Selected Editor Success'
}

export class LoadTags implements Action {
  readonly type = TagsActionTypes.LoadTags;
  constructor(public payload: TagsGetParams) {}
}

export class LoadTagsSuccess implements Action {
  readonly type = TagsActionTypes.LoadTagsSuccess;
  constructor(public payload: LoadedTagsModel) {}
}

export class LoadTagsFailure implements Action {
  readonly type = TagsActionTypes.LoadTagsFailure;
  constructor(public payload: any) {}
}

export class GetTag implements Action {
  readonly type = TagsActionTypes.GetTag;
  constructor(public payload: {entity: string, id: string, description?: string}) {}
}

export class GetTagSuccess implements Action {
  readonly type = TagsActionTypes.GetTagSuccess;
}

export class GetTagFailure implements Action {
  readonly type = TagsActionTypes.GetTagFailure;
  constructor(public payload: any) {}
}

export class DeleteTag implements Action {
  readonly type = TagsActionTypes.DeleteTag;
  constructor(public payload: {entity: string, id: string}) {}
}

export class DeleteTagSuccess implements Action {
  readonly type = TagsActionTypes.DeleteTagSuccess;
}

export class DeleteTagFailure implements Action {
  readonly type = TagsActionTypes.DeleteTagFailure;
  constructor(public payload: any) {}
}

export class SetSelected implements Action {
  readonly type = TagsActionTypes.SetSelected;
  constructor(public payload: {mode: string, data: string[]}) {}
}

export class SetSelectedEditorSuccess implements Action {
  readonly type = TagsActionTypes.SetSelectedEditorSuccess;
  constructor(public payload: string[]) {}
}

export class SetSelectedBrowserSuccess implements Action {
  readonly type = TagsActionTypes.SetSelectedBrowserSuccess;
  constructor(public payload: string[]) {}
}




export type TagsActions =
  | LoadTags
  | LoadTagsSuccess
  | LoadTagsFailure
  | GetTag
  | GetTagSuccess
  | GetTagFailure
  | DeleteTag
  | DeleteTagSuccess
  | DeleteTagFailure
  | SetSelected
  | SetSelectedEditorSuccess
  | SetSelectedBrowserSuccess;
