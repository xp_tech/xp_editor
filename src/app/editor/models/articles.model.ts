export interface ArticleModel {
  id: string; // maxLength: 64, pattern: /^\w+$/
  url: string; // maxLength: 128
  title: string; // maxLength: 255
  header: string; // maxLength: 255
  summary: string; // maxLength: 255
  description: string; // maxLength: 255
  keywords:	string; // maxLength: 255
  template: string;
  ogImage: {
    url: string; // maxLength: 255
    width: number; // >= 0
    height: number; // >= 0
  };
  locale: string; // from settings
  subdomain: string; // from settings
  published: boolean;
  body: string;
  tags: string[];
  createdAt?: string;
  updatedAt?: string;
}

export interface LoadedArticlesModel {
  count: number;
  rows: ArticleModel[];
}
