export interface FileModel {
    name: string;
    type: string;
    size?: string;
    mime?: string;
    createdAt?: string;
    updatedAt?: string;
}
