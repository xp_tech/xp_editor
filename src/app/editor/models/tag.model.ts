import {DefGetItemsParams} from '../../utils/router.utils';

export interface TagModel {
  id: string;
  description: string;
  entity: string;
  createdAt: string;
  updatedAt: string;
}

export interface LoadedTagsModel {
  count: number;
  rows: TagModel[];
}

export interface TagsGetParams extends DefGetItemsParams {
  entity: string;
}
