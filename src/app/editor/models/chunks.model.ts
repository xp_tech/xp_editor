export interface ChunkModel {
  id: string; // maxLength: 64, pattern: /^\w+$/
  description: string; // maxLength: 255
  body: string;
  tags: string[];
  createdAt?: string;
  updatedAt?: string;
}

export interface LoadedChunksModel {
  count: number;
  rows: ChunkModel[];
}
