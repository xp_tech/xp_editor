import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {User} from '../../user/user.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {AccountsLoadQueryParams} from '../models/accounts.model';
import {ActionsSubject, Store} from '@ngrx/store';
import {getCount, getRows, State as AccountsState} from '../reducers/accounts.reducer';
import {takeUntil} from 'rxjs/operators';
import {getError} from '../../editor/reducers/chunks.reducer';
import {AccountsActionTypes, ChangeRole, DeleteAccount, LoadAccounts} from '../actions/accounts.actions';
import {WarningSnackbarComponent} from '../../shared/components/warning-snackbar.component';


@Component({
  selector: 'app-roles',
  template: `
    <div fxLayout="row" fxLayoutGap="1em">
      <span class="mat-h3 mt-2">Filters:</span>
      <mat-form-field>
        <mat-label>Account role</mat-label>
        <mat-select #locSelect [(value)]="selectedRole" (selectionChange)="setFilter(selectedRole,'role')">
          <mat-option *ngFor="let i of ['all', 'guest', 'editor', 'admin']" value="{{i}}">{{i}}</mat-option>
        </mat-select>
      </mat-form-field>

      <mat-form-field class="example-form-field">
        <input matInput type="text" placeholder="Search email" #emailFilter
               (keypress)="searchEmail(emailFilter.value, $event.key)"
               (focusout)="searchEmail(emailFilter.value)"
        >
        <button mat-button *ngIf="emailFilter.value" matSuffix mat-icon-button aria-label="Clear" (click)="emailFilter.value='';searchEmail('')">
          <mat-icon>close</mat-icon>
        </button>
        <button mat-button *ngIf="emailFilter.value" matSuffix mat-icon-button aria-label="Search" (click)="searchEmail(emailFilter.value)">
          <mat-icon>search</mat-icon>
        </button>
      </mat-form-field>

    </div>

    <table mat-table [dataSource]="dataSource" matSort class="w-100">

      <ng-container matColumnDef="email">
        <th mat-header-cell  *matHeaderCellDef mat-sort-header> email </th>
        <td mat-cell *matCellDef="let element"> {{element.email}} </td>
      </ng-container>

      <ng-container matColumnDef="role">
        <th mat-header-cell class="w-200-px" *matHeaderCellDef>guest ---- editor ---- admin</th>
        <td mat-cell class="w-200-px" *matCellDef="let element">
          <mat-slider min="0" max="2" [value]="setRoleValue(element.role)" (valueChange)="setRole($event, element.email)"></mat-slider>
        </td>
      </ng-container>

      <ng-container matColumnDef="delete">
        <th mat-header-cell class="w-50-px text-center"  *matHeaderCellDef> delete </th>
        <td mat-cell class="w-50-px text-center" *matCellDef="let element">
          <button mat-icon-button color="warn" (click)="delete(element.email)">
            <mat-icon aria-label="Delete element">delete</mat-icon>
          </button>
        </td>
      </ng-container>


      <tr mat-header-row *matHeaderRowDef="displayedColumns; sticky: true"></tr>
      <tr mat-row *matRowDef="let row; columns: displayedColumns;"></tr>
    </table>
    <mat-paginator [length]="count"
                   [pageSize]="loadParams.limit"
                   [pageSizeOptions]="[5, 10, 25, 100]"
                   (page)="redefineParams($event)">
    </mat-paginator>
  `,
  styles: []
})
export class RolesComponent implements OnInit, OnDestroy {

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  componentDestroyed$: Subject<boolean> = new Subject();
  displayedColumns: string[] = ['email', 'role', 'delete'];
  _data: User[] = [];
  dataSource = new MatTableDataSource(this._data);

  count = 0;
  selectedRole = 'all';
  emailFilter: string;
  roles = ['guest', 'editor', 'admin'];

  loadParams: AccountsLoadQueryParams = {
    limit: '25',
    offset: '0'
  };

  constructor(private snackBar: MatSnackBar,
              private actionsSubj: ActionsSubject,
              private accountsStore: Store<AccountsState>) {
  }

  ngOnInit() {
    this.loadAccounts();

    this.accountsStore.select(getRows)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        this.dataSource =  new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
      });

    this.accountsStore.select(getCount)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => this.count = data);

    this.accountsStore.select(getError)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data) this.snackBar.open(data, undefined, {duration: 3000});
      });

    this.actionsSubj
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data.type === AccountsActionTypes.DeleteAccountSuccess) this.loadAccounts();
      });

  }

  loadAccounts() {
    this.accountsStore.dispatch(new LoadAccounts(this.loadParams));
  }

  redefineParams(event) {
    this.loadParams.limit = event.pageSize.toString();
    this.loadParams.offset = (event.pageSize * event.pageIndex).toString();
    this.loadAccounts();
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  setFilter(value, type) {
    (value === 'all' || value === '' || !value)
      ? delete this.loadParams[type]
      : this.loadParams[type] = value;
    this.loadAccounts();
  }

  searchEmail(v, k?) {
    if (k && k !== 'Enter') return;
    this.setFilter(v, 'filter');
  }

  delete(email) {
    const snackBarRef = this.snackBar.openFromComponent(WarningSnackbarComponent, {
      data: `You really want to delete this account?`,
      duration: 3000,
      panelClass: 'bg-grey'
    });
    snackBarRef.onAction()
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(() => {
        this.accountsStore.dispatch(new DeleteAccount(email));
      });
  }

  setRoleValue(role) {
    return this.roles.indexOf(role);
  }

  setRole(e, email) {
    this.accountsStore.dispatch(new ChangeRole({role: this.roles[e], email}));
  }

}
