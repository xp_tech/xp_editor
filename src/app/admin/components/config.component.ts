import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {getSetup, State as SettingsState} from '../reducers/settings.reducer';
import {LoadSettings} from '../actions/settings.actions';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-config',
  template: `
    <mat-form-field class="d-block w-100">
              <textarea matInput cdkTextareaAutosize placeholder="Locales"
                        [formControl]="localesFC"
                        (focusout)="save()"></textarea>
      <mat-error
        *ngIf="localesFC.hasError('pattern') || localesFC.hasError('required')">
        Not a valid set (letters, space and , only)
      </mat-error>
    </mat-form-field>

    <mat-form-field class="d-block w-100">
              <textarea matInput cdkTextareaAutosize placeholder="Regions"
                        [formControl]="regionsFC"
                        (focusout)="save()"></textarea>
      <mat-error
        *ngIf="regionsFC.hasError('pattern') || regionsFC.hasError('required')">
        Not a valid set (letters, space and , only)
      </mat-error>
    </mat-form-field>


    <mat-form-field class="d-block w-100">
              <textarea matInput cdkTextareaAutosize placeholder="Subdomains"
                        maxlength="255" [formControl]="subdomainsFC"
                        (focusout)="save()"></textarea>
      <mat-error
        *ngIf="subdomainsFC.hasError('pattern') || subdomainsFC.hasError('required')">
        Not a valid set (@, letters, space and , only)
      </mat-error>
    </mat-form-field>
    
    <div fxLayout="row" fxLayoutGap="1em" fxLayoutAlign="start center">
      <button mat-mini-fab class="mx-1" (click)="createNew()">
        <mat-icon aria-label="Create new">add</mat-icon>
      </button>
      <h1 class="m-0">Mirrors:</h1>
    </div>
    
    <div *ngFor="let m of mirrors; let i = index" fxLayout="row" fxLayoutGap="1em" fxLayoutAlign="start center">
      <mat-form-field fxFlex>
        <input matInput placeholder="host" value="{{m[0]}}">
      </mat-form-field>

      <mat-form-field>
        <input matInput placeholder="locale" value="{{m[1].split('-')[0]}}">
      </mat-form-field>

      <mat-form-field>
        <input matInput placeholder="region" value="{{m[1].split('-')[1]}}">
      </mat-form-field>

      <button mat-icon-button (click)="delete()">
        <mat-icon aria-label="delete mirror">delete</mat-icon>
      </button>
    </div>
    
  `,
  styles: []
})
export class ConfigComponent implements OnInit, OnDestroy {

  componentDestroyed$: Subject<boolean> = new Subject();
  mirrors = [['', '-']];
  locales: string[];
  regions: string[];

  localesFC = new FormControl('', [
    Validators.required,
    Validators.pattern('(([a-z])\\w+(\\,?\\s?))+'),
  ]);

  regionsFC = new FormControl('', [
    Validators.required,
    Validators.pattern('(([a-z])\\w+(\\,?\\s?))+'),
  ]);

  subdomainsFC = new FormControl('', [
    Validators.required,
    Validators.pattern('(([@a-z])\\w+(\\,?\\s?))+'),
  ]);

  constructor(private settingsStore: Store<SettingsState>) { }

  ngOnInit() {
    this.settingsStore.dispatch(new LoadSettings());

    this.settingsStore.select(getSetup)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data) {
          this.localesFC.setValue(data.locales.join(', '));
          this.subdomainsFC.setValue(data.subdomains.join(', '));
          this.regionsFC.setValue( data.regions.join(', '));
          this.mirrors = data.mirrors;
        }
      });


  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  save() {

  }

  createNew() {

  }

  delete() {

  }

}
