import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-settings',
  template: `
    <section fxLayout="row" fxLayoutGap="2em">
      <div fxFlex="80">
        <mat-tab-group>
          <mat-tab label="Config">
            <app-config></app-config>
          </mat-tab>
          <mat-tab label="Roles">
            <app-roles></app-roles>
          </mat-tab>
        </mat-tab-group>
      </div>

      <div fxFlex="20">
        <h1>NB!</h1>
        <p>After role change, user should sign out manually.</p>
      </div>
    </section>
  `,
  styles: []
})
export class SettingsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
