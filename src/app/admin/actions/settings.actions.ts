import { Action } from '@ngrx/store';
import {SettingsModel} from '../models/settings.model';

export enum SettingsActionTypes {
  LoadSettings = '[Settings] Load Settings',
  LoadSettingsSuccess = '[Settings] Load Settings Success',
  LoadSettingsFailure = '[Settings] Load Settings Failure',
}

export class LoadSettings implements Action {
  readonly type = SettingsActionTypes.LoadSettings;
}

export class LoadSettingsSuccess implements Action {
  readonly type = SettingsActionTypes.LoadSettingsSuccess;
  constructor(public payload: SettingsModel) {}
}

export class LoadSettingsFailure implements Action {
  readonly type = SettingsActionTypes.LoadSettingsFailure;
  constructor(public payload: any) {}
}


export type SettingsActions =
  | LoadSettings
  | LoadSettingsSuccess
  | LoadSettingsFailure;

