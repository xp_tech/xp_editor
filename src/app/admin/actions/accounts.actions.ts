import { Action } from '@ngrx/store';
import {User} from '../../user/user.model';
import {AccountsLoadQueryParams} from '../models/accounts.model';

export enum AccountsActionTypes {
  LoadAccounts = '[Accounts] Load Accounts',
  LoadAccountsSuccess = '[Accounts] Load Accounts Success',
  LoadAccountsFailure = '[Accounts] Load Accounts Failure',

  ChangeRole = '[Accounts] Change Role',
  ChangeRoleSuccess = '[Accounts] Change Role Success',
  ChangeRoleFailure = '[Accounts] Change Role Failure',

  DeleteAccount = '[Accounts] Delete Account',
  DeleteAccountSuccess = '[Accounts] Delete Account Success',
  DeleteAccountFailure = '[Accounts] Delete Account Failure',
}

export class LoadAccounts implements Action {
  readonly type = AccountsActionTypes.LoadAccounts;
  constructor(public payload: AccountsLoadQueryParams) {}
}

export class LoadAccountsSuccess implements Action {
  readonly type = AccountsActionTypes.LoadAccountsSuccess;
  constructor(public payload: {count: number, rows: User[]}) {}
}

export class LoadAccountsFailure implements Action {
  readonly type = AccountsActionTypes.LoadAccountsFailure;
  constructor(public payload: any) {}
}

export class ChangeRole implements Action {
  readonly type = AccountsActionTypes.ChangeRole;
  constructor(public payload: {role: string, email: string}) {}
}

export class ChangeRoleSuccess implements Action {
  readonly type = AccountsActionTypes.ChangeRoleSuccess;
}

export class ChangeRoleFailure implements Action {
  readonly type = AccountsActionTypes.ChangeRoleFailure;
  constructor(public payload: any) {}
}

export class DeleteAccount implements Action {
  readonly type = AccountsActionTypes.DeleteAccount;
  constructor(public payload: string) {}
}

export class DeleteAccountSuccess implements Action {
  readonly type = AccountsActionTypes.DeleteAccountSuccess;
}

export class DeleteAccountFailure implements Action {
  readonly type = AccountsActionTypes.DeleteAccountFailure;
  constructor(public payload: any) {}
}


export type AccountsActions =
  | LoadAccounts
  | LoadAccountsSuccess
  | LoadAccountsFailure
  | DeleteAccount
  | DeleteAccountFailure
  | DeleteAccountSuccess
  | ChangeRole
  | ChangeRoleFailure
  | ChangeRoleSuccess;

