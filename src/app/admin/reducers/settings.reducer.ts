import {SettingsModel} from '../models/settings.model';
import {SettingsActions, SettingsActionTypes} from '../actions/settings.actions';
import {createFeatureSelector, createSelector} from '@ngrx/store';


export interface State {
  setup?: SettingsModel;
  error?: string;
}

export const initialState: State = {};

export function reducer(state = initialState, action: SettingsActions): State {
  switch (action.type) {

    case SettingsActionTypes.LoadSettings: {
      return state;
    }

    case SettingsActionTypes.LoadSettingsSuccess: {
      return {
        ...state,
        setup: action.payload
      };
    }

    case SettingsActionTypes.LoadSettingsFailure: {
      return  {
        ...state,
        error: action.payload.message
      };
    }

    default:
      return state;
  }
}

export const selectFeatureState = createFeatureSelector<State>('settingsState');
export const getSetup = createSelector(
  selectFeatureState,
  (state: State) => state.setup
);
export const getError = createSelector(
  selectFeatureState,
  (state: State) => state.error
);
