import {createFeatureSelector, createSelector} from '@ngrx/store';
import {User} from '../../user/user.model';
import {AccountsActions, AccountsActionTypes} from '../actions/accounts.actions';


export interface State {
  rows?: User[];
  count: number;
  error?: string;
}

export const initialState: State = {
  count: 0
};

export function reducer(state = initialState, action: AccountsActions): State {
  switch (action.type) {

    case AccountsActionTypes.LoadAccounts: {
      return state;
    }

    case AccountsActionTypes.LoadAccountsSuccess: {
      return {
        ...state,
        count: action.payload.count,
        rows: action.payload.rows
      };
    }

    case AccountsActionTypes.LoadAccountsFailure: {
      return  {
        ...state,
        error: action.payload.message
      };
    }

    case AccountsActionTypes.ChangeRole: {
      return state;
    }

    case AccountsActionTypes.ChangeRoleSuccess: {
      return state;
    }

    case AccountsActionTypes.ChangeRoleFailure: {
      return state;
    }

    case AccountsActionTypes.DeleteAccount: {
      return state;
    }

    case AccountsActionTypes.DeleteAccountSuccess: {
      return state;
    }

    case AccountsActionTypes.DeleteAccountFailure: {
      return state;
    }

    default:
      return state;
  }
}

export const selectFeatureState = createFeatureSelector<State>('accountsState');
export const getCount = createSelector(
  selectFeatureState,
  (state: State) => state.count
);
export const getRows = createSelector(
  selectFeatureState,
  (state: State) => state.rows
);
export const getError = createSelector(
  selectFeatureState,
  (state: State) => state.error
);
