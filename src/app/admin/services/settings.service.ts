import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Server} from '../../utils/router.utils';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  constructor(private http: HttpClient) { }

  get() {
    return this.http.get(Server.api(`admin/settings`), {withCredentials: true});
  }
}
