import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Server} from '../../utils/router.utils';
import {AccountsLoadQueryParams} from '../models/accounts.model';
import {User} from '../../user/user.model';

@Injectable({
  providedIn: 'root'
})
export class AccountsService {

  constructor(private http: HttpClient) { }

  getAll(params: AccountsLoadQueryParams) {
    const options = {
      params: {
        limit: params.limit || '20',
        offset: params.offset || '0',
      },
      withCredentials: true
    };
    for (const i of ['desc', 'role', 'email', 'filter']) {
      if (params[i]) (options.params as any)[i] = params[i];
    }

    return this.http.get(Server.api('admin/users'), options);
  }

  update(item: User) {
    return this.http.post(Server.api(`admin/users`),
      {
        role: item.role,
        email: item.email,
      }, {
        withCredentials: true,
        responseType: 'text'
      });
  }

  delete(email: string) {
    return this.http.delete(Server.api(`admin/users?email=${email}`), {
      withCredentials: true,
      responseType: 'text'
    });
  }

}
