import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialModule} from '../material/material.module';

import {StoreModule} from '@ngrx/store';
import * as fromSettings from './reducers/settings.reducer';
import * as fromAccounts from './reducers/accounts.reducer';

import {EffectsModule} from '@ngrx/effects';
import {SettingsEffects} from './effects/settings.effects';
import {AccountsEffects} from './effects/accounts.effects';

import { SettingsComponent } from './components/settings.component';
import { RolesComponent } from './components/roles.component';
import { ConfigComponent } from './components/config.component';
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [SettingsComponent, RolesComponent, ConfigComponent],
  imports: [
    CommonModule,
    MaterialModule,
    StoreModule.forFeature('settingsState', fromSettings.reducer),
    StoreModule.forFeature('accountsState', fromAccounts.reducer),
    EffectsModule.forFeature([SettingsEffects, AccountsEffects]),
    ReactiveFormsModule
  ]
})
export class AdminModule { }
