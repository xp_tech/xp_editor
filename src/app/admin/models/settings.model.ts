export interface SettingsModel {
  subdomains: string[];
  locales: string[];
  regions: string[];
  mirrors: string[][];
}
