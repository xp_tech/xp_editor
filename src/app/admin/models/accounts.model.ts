export interface AccountsLoadQueryParams {
  limit: string;
  offset: string;
  desc?: string;
  role?: string;
  email?: string;
  filter?: string;
}
