import { Injectable } from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Observable, of} from 'rxjs';
import {Action} from '@ngrx/store';
import {catchError, switchMap, map} from 'rxjs/operators';
import {AccountsService} from '../services/accounts.service';
import {
  AccountsActionTypes,
  DeleteAccount, DeleteAccountFailure, DeleteAccountSuccess,
  LoadAccounts,
  LoadAccountsFailure,
  LoadAccountsSuccess,
  ChangeRole, ChangeRoleSuccess, ChangeRoleFailure
} from '../actions/accounts.actions';
import {User} from '../../user/user.model';



@Injectable()
export class AccountsEffects {

  constructor(private actions$: Actions,
              private accountsService: AccountsService) {}

  @Effect()
  loadAccounts: Observable<Action> = this.actions$
    .pipe(
      ofType(AccountsActionTypes.LoadAccounts),
      switchMap((action: LoadAccounts) => this.accountsService.getAll(action.payload)
        .pipe(
          map((data: {count: number, rows: User[]}) => new LoadAccountsSuccess({count: data.count, rows: data.rows})),
          catchError(err => of(new LoadAccountsFailure(err)))
        ))
    );

  @Effect()
  deleteAccount: Observable<Action> = this.actions$
    .pipe(
      ofType(AccountsActionTypes.DeleteAccount),
      switchMap((action: DeleteAccount) => this.accountsService.delete(action.payload)
        .pipe(
          map(data => new DeleteAccountSuccess()),
          catchError(err => of(new DeleteAccountFailure(err)))
        ))
    );

  @Effect()
  changeRole: Observable<Action> = this.actions$
    .pipe(
      ofType(AccountsActionTypes.ChangeRole),
      switchMap((action: ChangeRole) => this.accountsService.update(action.payload)
        .pipe(
          map(data => new ChangeRoleSuccess()),
          catchError(err => of(new ChangeRoleFailure(err)))
        ))
    );

}
