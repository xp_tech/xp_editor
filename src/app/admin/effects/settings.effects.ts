import { Injectable } from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Observable, of} from 'rxjs';
import {Action} from '@ngrx/store';
import {LoadSettingsFailure, LoadSettingsSuccess, SettingsActionTypes} from '../actions/settings.actions';
import {catchError, switchMap, map} from 'rxjs/operators';
import {SettingsService} from '../services/settings.service';
import {SettingsModel} from '../models/settings.model';



@Injectable()
export class SettingsEffects {

  constructor(private actions$: Actions,
              private settingsService: SettingsService) {}

  @Effect()
  loadSettings: Observable<Action> = this.actions$
    .pipe(
      ofType(SettingsActionTypes.LoadSettings),
      switchMap(action => this.settingsService.get()
        .pipe(
          map((data: SettingsModel) => new LoadSettingsSuccess(data)),
          catchError(err => of(new LoadSettingsFailure(err)))
        ))
    );

}
