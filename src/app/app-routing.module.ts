import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './editor/components/dashboard.component';
import {UserGuard, EditorGuard, AdminGuard} from './user/user.guard';
import {NotFoundComponent} from './not-found.component';
import {ChunksComponent} from './editor/components/chunks.component';
import {PagesComponent} from './editor/components/pages.component';
import {ArticlesComponent} from './editor/components/articles.component';
import {TestsComponent} from './editor/components/tests.component';
import {CoursesComponent} from './editor/components/courses.component';
import {FilesComponent} from './editor/components/files.component';
import {SettingsComponent} from './admin/components/settings.component';
import {EntranceComponent} from './user/components/entrance.component';

// TODO: fill guards
const routes: Routes = [
  {path: '', component: DashboardComponent, canActivate: [UserGuard] },
  {path: 'enter', component: EntranceComponent},
  {path: 'chunks', component: ChunksComponent, canActivate: [] },
  {path: 'pages', component: PagesComponent, canActivate: [] },
  {path: 'articles', component: ArticlesComponent, canActivate: [] },
  {path: 'tests', component: TestsComponent, canActivate: [] },
  {path: 'courses', component: CoursesComponent, canActivate: [] },
  {path: 'files', component: FilesComponent, canActivate: [] },
  {path: 'settings', component: SettingsComponent, canActivate: [] },
  {path: '404', component: NotFoundComponent},
  {path: '**', redirectTo: '/404'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
