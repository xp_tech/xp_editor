import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActionsSubject, Store} from '@ngrx/store';
import {getError, getUser, State as UserState} from './user/user.reducer';
import {LoadUser} from './user/user.actions';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  template: `
    <app-entrance *ngIf="user === 'none'"></app-entrance>
    <div *ngIf="user && user !== 'none'">
      <app-header></app-header>
      <div class="my-1 mx-auto w-1200">
        <router-outlet></router-outlet>
      </div>
    </div>
  `,
  styles: []
})
export class AppComponent implements OnInit, OnDestroy {
  user: string;
  componentDestroyed$: Subject<boolean> = new Subject();

  constructor(
    private userStore: Store<UserState>,
    private router: Router,
    private actionsSubj: ActionsSubject
  ) {
  }

  ngOnInit() {
    this.userStore.dispatch(new LoadUser());

    this.userStore.select(getUser)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data && !data.email) { this.user = 'none'; }
        if (data && data.email) { this.user = data.email; }
        if (this.user && this.user !== 'none') { this.router.navigate([this.router.url]); }
      });

    this.userStore.select(getError)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
        if (data) { this.user = 'none'; }
      });

  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

}
