import { Action } from '@ngrx/store';

export enum QueryActionTypes {
  StartQuery = '[Query] Start Query',
  FinishQuery = '[Query] Finish Query',
  
  
}

export class StartQuery implements Action {
  readonly type = QueryActionTypes.StartQuery;
}

export class FinishQuery implements Action {
  readonly type = QueryActionTypes.FinishQuery;
}


export type QueryActions = | StartQuery | FinishQuery;
