import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header.component';
import {RouterModule} from '@angular/router';
import {MaterialModule} from '../material/material.module';

import {StoreModule} from '@ngrx/store';
import * as fromQuery from './reducers/query.reducer';

import { WarningSnackbarComponent } from './components/warning-snackbar.component';



@NgModule({
  declarations: [HeaderComponent, WarningSnackbarComponent],
  exports: [
    HeaderComponent,
    WarningSnackbarComponent
  ],
  entryComponents: [WarningSnackbarComponent],
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    StoreModule.forFeature('queryState', fromQuery.reducer)
  ],
  providers: []
})
export class SharedModule { }
