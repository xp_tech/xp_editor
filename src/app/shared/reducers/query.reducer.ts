import {Action, createFeatureSelector, createSelector} from '@ngrx/store';
import {QueryActionTypes} from '../actions/query.actions';


export interface State {
  query: boolean;
}

export const initialState: State = {
  query: false
};

export const positiveState: State = {
  query: true
};

export function reducer(state = initialState, action: Action): State {
  switch (action.type) {

    case QueryActionTypes.StartQuery: {
      return positiveState;
    }

    case QueryActionTypes.FinishQuery: {
      return initialState;
    }

    default:
      return state;
  }
}

export const selectFeatureState = createFeatureSelector<State>('queryState');
export const getState = createSelector(
  selectFeatureState,
  (state: State) => state.query
);
