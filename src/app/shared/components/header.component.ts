import {Component, OnDestroy, OnInit} from '@angular/core';
import {SignOut} from '../../user/user.actions';
import {Store} from '@ngrx/store';
import {getUser, State as UserState} from '../../user/user.reducer';
import {getState, State as QueryState} from '../reducers/query.reducer';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {FinishQuery, StartQuery} from '../actions/query.actions';

@Component({
  selector: 'app-header',
  template: `
    <header class="bg-grey">
      <div fxLayout="row" fxLayoutAlign="space-between center" class="mx-auto w-1200">
        <img src="/assets/logo.svg" class="logo my-1">
        <button mat-button routerLinkActive="bg-primary" [routerLinkActiveOptions]="{exact:true}" routerLink="/">Dashboard</button>
        <button mat-button *ngIf="editor" routerLinkActive="bg-primary" [routerLinkActiveOptions]="{exact:true}" routerLink="/chunks">Chunks</button>
        <button mat-button *ngIf="editor" routerLinkActive="bg-primary" [routerLinkActiveOptions]="{exact:true}" routerLink="/pages">Pages</button>
        <button mat-button *ngIf="editor" routerLinkActive="bg-primary" [routerLinkActiveOptions]="{exact:true}" routerLink="/articles">Articles</button>
        <!--button mat-button *ngIf="editor" routerLinkActive="bg-primary" [routerLinkActiveOptions]="{exact:true}" routerLink="/tests">Tests</button>
        <button mat-button *ngIf="editor" routerLinkActive="bg-primary" [routerLinkActiveOptions]="{exact:true}" routerLink="/courses">Courses</button-->
        <button mat-button *ngIf="editor" routerLinkActive="bg-primary" [routerLinkActiveOptions]="{exact:true}" routerLink="/files">Files</button>
        <button mat-button *ngIf="admin" routerLinkActive="bg-primary" [routerLinkActiveOptions]="{exact:true}" routerLink="/settings">Settings</button>
        <button mat-button (click)="Signout()" color="warn">Signout</button>
      </div>
      <mat-progress-bar mode="query" [fxHide]="hideProgress"></mat-progress-bar>
    </header>
  `,
  styles: [`
    header {
      position: relative;
    }
    mat-progress-bar {
      position: absolute;
      bottom: 0;
    }
  .logo {
    display: inline-block;
    height: 70px;
  }
    button {
      margin-bottom: 20px;
    }
  `]
})
export class HeaderComponent implements OnInit, OnDestroy {

  hideProgress: boolean;
  editor: boolean;
  admin: boolean;
  componentDestroyed$: Subject<boolean> = new Subject();

  constructor(private userStore: Store<UserState>,
              private queryStore: Store<QueryState>) { }

  ngOnInit() {
    const qs = this.queryStore;

    const origOpen = XMLHttpRequest.prototype.open;
    XMLHttpRequest.prototype.open = function() {
      qs.dispatch(new StartQuery());
      this.addEventListener('load', () => qs.dispatch(new FinishQuery()));
      origOpen.apply(this, arguments);
    };

    qs.select(getState)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(d => this.hideProgress = !d);

    this.userStore.select(getUser)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(u => {
        const roles = ['editor', 'admin'];
        if (roles.includes(u.role)) this.editor = true;
        this.admin = u.role === roles[1];
      });
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  Signout() {
    this.userStore.dispatch(new SignOut());
  }

}
