import {Component, Inject, OnInit} from '@angular/core';
import { MAT_SNACK_BAR_DATA, MatSnackBarRef } from '@angular/material/snack-bar';

@Component({
  selector: 'app-warning-snackbar',
  template: `
    <div class="w-100">
      <p> {{ data }} </p>
      <div class="text-right">
        <button mat-button (click)="clear()">Cancel</button>
        <button mat-button color="warn" (click)="action()">DO IT!</button>
      </div>

    </div>
  `,
  styles: []
})
export class WarningSnackbarComponent implements OnInit {

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any,
              private snackbarRef: MatSnackBarRef<WarningSnackbarComponent>) { }

  ngOnInit() {
  }

  clear(): void {
    this.snackbarRef.dismiss();
  }

  action(): void {
    this.snackbarRef.dismissWithAction();
  }
}
