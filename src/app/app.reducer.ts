import {
  ActionReducer,
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';
import * as fromUser from './user/user.reducer';
import { RouterStateUrl } from './utils/router.utils';
import { environment } from '../environments/environment';

export interface State {
  routerState: fromRouter.RouterReducerState<RouterStateUrl>;
  userState: fromUser.State;
}

export const reducers: ActionReducerMap<State> = {
  routerState: fromRouter.routerReducer,
  userState: fromUser.reducer
};



// console.log all actions
export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
  return function(state: State, action: any): State {
    console.log('state', state);
    console.log('action', action);

    return reducer(state, action);
  };
}

export const metaReducers: MetaReducer<State>[] = !environment.production
  ? [logger]
  : [];
