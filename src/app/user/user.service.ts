import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {expand, map} from 'rxjs/operators';

import {Server} from '../utils/router.utils';

import {User} from './user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  _setUserData = (data) => {
    return {
      email: data.email,
      role: data.role
    };
  }

  loadUser(): Observable<false | User> {
    return this.http
      .get<{ email: string, role: string }>(Server.api('entrance/whoami'), {withCredentials: true})
      .pipe(
        map(data => (data.email) ? this._setUserData(data) : undefined)
      );
  }

  signup(body): Observable<string> {
    return this.http
      .post<string>(Server.api('entrance/signup'), body, {withCredentials: true});
  }

  signin(body): Observable<User> {
    return this.http
      .post<string>(Server.api('entrance/signin'), body, {withCredentials: true})
      .pipe(
        map(data => this._setUserData(data))
      );
  }

  signout() {
    return this.http
      .post<string>(Server.api('entrance/signout'), {}, {withCredentials: true, responseType: 'text' as 'json'});
  }

  recover(body): Observable<string> {
    return this.http
      .get<string>(Server.api(`entrance/recover?email=${body.email}`), {withCredentials: true, responseType: 'text' as 'json'});
  }

  setNewPassword(body): Observable<string> {
    return this.http
      .put<string>(Server.api('entrance/recover'), body, {withCredentials: true, responseType: 'text' as 'json'});
  }
}
