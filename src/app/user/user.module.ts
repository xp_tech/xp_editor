import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromUser from './user.reducer';
import { EntranceComponent } from './components/entrance.component';
import {MaterialModule} from '../material/material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {EffectsModule} from '@ngrx/effects';

import { SigninFormComponent } from './components/signin-form.component';
import { SignupFormComponent } from './components/signup-form.component';
import { RecoverFormComponent } from './components/recover-form.component';
import {UserEffects} from './user.effects';


@NgModule({
  declarations: [EntranceComponent, SigninFormComponent, SignupFormComponent, RecoverFormComponent],
  exports: [
    EntranceComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forFeature('userState', fromUser.reducer),
    EffectsModule.forFeature([UserEffects])
  ]
})
export class UserModule { }
