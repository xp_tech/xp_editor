import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {State as UserState} from '../user.reducer';
import {Subject, Subscription} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-entrance',
  template: `
    <div fxLayout="column" fxLayoutAlign="center center">
      <div>
        <img src="/assets/logo.svg" class="logo my-1 d-block mx-auto">
      </div>
      <div>
        <mat-tab-group [selectedIndex]="selectedIndex">
          <mat-tab label="Sign in">
            <app-signin-form></app-signin-form>
          </mat-tab>

          <mat-tab label="Sign up">
            <app-signup-form [email]="recoverEmail" [code]="recoverCode"></app-signup-form>
          </mat-tab>

          <mat-tab label="Recover">
            <app-recover-form></app-recover-form>
          </mat-tab>
        </mat-tab-group>
      </div>
    </div>
  `,
  styles: [`
  .logo {
    height: 100px;
  }
  `]
})
export class EntranceComponent implements OnInit, OnDestroy {

  componentDestroyed$: Subject<boolean> = new Subject();

  recoverCode: string;
  recoverEmail: string;
  selectedIndex = 0;
  routeSub: Subscription;

  constructor(private route: ActivatedRoute,
              private userStore: Store<UserState>,
              private router: Router) {
  }

  ngOnInit() {

    this.routeSub = this.route.queryParams
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(params => {
        this.recoverCode = params.recoverCode;
        this.recoverEmail = params.recoverEmail;
        if (this.recoverCode) { this.selectedIndex = 1; }
      });

  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }


}
