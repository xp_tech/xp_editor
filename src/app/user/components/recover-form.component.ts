import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';
import {State as UserState} from '../user.reducer';
import {Recover} from '../user.actions';

@Component({
  selector: 'app-recover-form',
  template: `
    <p class="text-center mt-1">Please, fill in the email you've registered with.</p>
    <form fxLayout="column" [formGroup]="form">

      <mat-form-field>
        <input matInput placeholder="Email" formControlName="email">
        <mat-hint>Real email please</mat-hint>
        <mat-error *ngIf="form.get('email').hasError('email') && !form.get('email').hasError('required')">
          Please enter a valid email address
        </mat-error>
        <mat-error *ngIf="form.get('email').hasError('required')">
          Email is <strong>required</strong>
        </mat-error>
      </mat-form-field>

      <div class="text-right">
        <button mat-raised-button color="accent" (click)="Submit()">Send</button>
      </div>

    </form>
  `,
  styles: []
})
export class RecoverFormComponent implements OnInit {

  form = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.email,
    ]),
  });

  constructor(private userStore: Store<UserState>) { }

  ngOnInit() {
  }

  Submit() {
    if (this.form.valid) {
      this.userStore.dispatch(new Recover({
        email: this.form.get('email').value
      }));
    }
  }

}
