import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActionsSubject, Store} from '@ngrx/store';
import {State as UserState} from '../user.reducer';
import {SignIn, UserActionTypes} from '../user.actions';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-signin-form',
  template: `
    <form fxLayout="column" [formGroup]="form">

      <mat-form-field>
        <input matInput placeholder="Email" formControlName="email">
        <mat-hint>Real email please</mat-hint>
        <mat-error *ngIf="form.get('email').hasError('email') && !form.get('email').hasError('required')">
          Please enter a valid email address
        </mat-error>
        <mat-error *ngIf="form.get('email').hasError('required')">
          Email is <strong>required</strong>
        </mat-error>
      </mat-form-field>

      <mat-form-field>
        <input matInput type="password" placeholder="Password" formControlName="password">
        <mat-error *ngIf="form.get('password').hasError('required')">
          Password is <strong>required</strong>
        </mat-error>
      </mat-form-field>

      <div class="text-right">
        <button mat-raised-button color="accent" (click)="Submit()">Sign in</button>
      </div>

    </form>
  `,
  styles: []
})
export class SigninFormComponent implements OnInit, OnDestroy {

  componentDestroyed$: Subject<boolean> = new Subject();

  form = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.email,
    ]),

    password: new FormControl('', [
      Validators.required,
    ])
  });

  lastEmail: string;
  error = false;

  constructor(
    private userStore: Store<UserState>,
    private actionsSubj: ActionsSubject
  ) {
  }

  ngOnInit() {
    this.actionsSubj
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
      if (data.type === UserActionTypes.SignInFailure) {
        this.lastEmail = this.form.get('email').value;
        this.form.reset();
        this.error = true;
      } else { this.error = false; }
    });
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  Submit() {
    if (this.form.valid) {
      const body = {
        email: this.form.get('email').value,
        password: this.form.get('password').value,
      };
      this.userStore.dispatch(new SignIn(body));
    }
  }
}
