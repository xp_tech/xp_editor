import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Store, ActionsSubject} from '@ngrx/store';
import {State as UserState} from '../user.reducer';
import {NewPassword, SignUp, UserActionTypes} from '../user.actions';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-signup-form',
  template: `
    <p class="text-center mt-1" *ngIf="code">Please enter you new password</p>
    <form fxLayout="column" [formGroup]="form">
      <mat-form-field>
        <input matInput placeholder="Email" formControlName="email">
        <mat-hint>Real email please</mat-hint>
        <mat-error *ngIf="form.get('email').hasError('email') && !form.get('email').hasError('required')">
          Please enter a valid email address
        </mat-error>
        <mat-error *ngIf="form.get('email').hasError('required')">
          Email is <strong>required</strong>
        </mat-error>
      </mat-form-field>

      <mat-form-field>
        <input matInput type="password" placeholder="Password" formControlName="password">
        <mat-error *ngIf="form.get('password').hasError('required')">
          Password is <strong>required</strong>
        </mat-error>
      </mat-form-field>

      <mat-form-field>
        <input matInput type="password" placeholder="Repeat password" formControlName="passwordRep">
        <mat-error *ngIf="form.get('passwordRep').hasError('required')">
          Password repeat is <strong>required</strong>
        </mat-error>
        <mat-error *ngIf="form.get('passwordRep').hasError('matchOther') && !form.get('passwordRep').hasError('required')">
          Passwords don't match
        </mat-error>
      </mat-form-field>

      <div class="text-right">
        <button mat-raised-button color="accent" (click)="Submit()">Send</button>
      </div>
    </form>
    <p class="text-center text-accent" *ngIf="status==='success'">User {{lastEmail}} registered</p>
    <p class="text-center text-accent" *ngIf="status==='newPasswordSuccess'">User {{lastEmail}} password updated</p>
    <p class="text-center text-warn" *ngIf="status==='failure'">Sorry, operation failed due to server error</p>
  `,
  styles: []
})
export class SignupFormComponent implements OnInit, OnDestroy {

  componentDestroyed$: Subject<boolean> = new Subject();

  form = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.email,
    ]),

    password: new FormControl('', [
      Validators.required,
    ]),

    passwordRep: new FormControl('', [
      Validators.required, this.matchOtherValidator('password')
    ])
  });

  status: string;
  lastEmail: string;

  @Input('email') email: string;
  @Input('code') code: string;


  constructor(
    private userStore: Store<UserState>,
    private actionsSubj: ActionsSubject
  ) {
  }

  ngOnInit() {
    if (this.code) {
      const em = this.form.get('email');
      em.setValue(this.email);
      console.log(em.value);
    }

    this.actionsSubj
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(data => {
      if (data.type === UserActionTypes.SignUpSuccess) {
        this.lastEmail = this.form.get('email').value;
        this.form.reset();
        this.status = 'success';
      } else if (data.type === UserActionTypes.NewPasswordSuccess) {
        this.lastEmail = this.email;
        this.form.reset();
        this.status = 'newPasswordSuccess';
      } else if (data.type === UserActionTypes.SignUpFailure || data.type === UserActionTypes.NewPasswordFailure) {
        this.status = 'failure';
      } else { this.status = undefined; }
    });
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  Submit() {
    if (this.form.valid) {
      const body = {
        email: this.form.get('email').value,
        password: this.form.get('password').value,
        code: this.code
      };
      if (this.code) { this.userStore.dispatch(new NewPassword(body)); } else { this.userStore.dispatch(new SignUp(body)); }
    }
  }

  matchOtherValidator(otherControlName: string) {

    let thisControl: FormControl;
    let otherControl: FormControl;

    return function matchOtherValidate(control: FormControl) {

      if (!control.parent) {
        return null;
      }

      // Initializing the validator.
      if (!thisControl) {
        thisControl = control;
        otherControl = control.parent.get(otherControlName) as FormControl;
        if (!otherControl) {
          throw new Error('matchOtherValidator(): other control is not found in parent group');
        }
        otherControl.valueChanges
          .subscribe(() => {
          thisControl.updateValueAndValidity();
        });
      }

      if (!otherControl) {
        return null;
      }

      if (otherControl.value !== thisControl.value) {
        return {
          matchOther: true
        };
      }

      return null;

    };

  }

}
