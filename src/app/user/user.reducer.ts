import { User } from './user.model';
import { UserActions, UserActionTypes } from './user.actions';
import {createFeatureSelector, createSelector} from '@ngrx/store';


export interface State {
  error?: string;
  user?: User;
}

export const initialState: State = {

};

export function reducer(state = initialState, action: UserActions): State {
  switch (action.type) {

    case UserActionTypes.LoadUser: {
      return state;
    }

    case UserActionTypes.SignIn: {
      return state;
    }

    case UserActionTypes.SignOut: {
      return state;
    }

    case UserActionTypes.SignUp: {
      return state;
    }

    case UserActionTypes.SignInFailure: {
      return {
        ...state,
        error: action.payload.message,
      };
    }

    case UserActionTypes.SignInSuccess: {
      return {
        ...state,
        user: action.payload,
      };
    }

    case UserActionTypes.SignOutFailure: {
      return {
        ...state,
        error: action.payload.message,
      };
    }

    case UserActionTypes.SignOutSuccess: {
      return {
        ...state,
        user: {
          email: undefined,
          role: undefined
        }
      };
    }

    case UserActionTypes.SignUpFailure: {
      return {
        ...state,
        error: action.payload.message,
      };
    }

    case UserActionTypes.SignUpSuccess: {
      return initialState;
    }

    default:
      return state;
  }
}

export const selectUsersState = createFeatureSelector<State>('userState');
export const getUser = createSelector(
  selectUsersState,
  (state: State) => state.user
);
export const getError = createSelector(
  selectUsersState,
  (state: State) => state.error
);
