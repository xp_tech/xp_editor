import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import {Store} from '@ngrx/store';
import {getUser, State as UserState} from './user.reducer';
import {take, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate {

  constructor(
    private userStore: Store<UserState>,
    private router: Router) {}

  canActivate(): Observable<boolean> {
    return this.userStore
      .select(getUser)
      .pipe(
        map(u => {
          return !!(u && u.email);
        }),
        take(1)
      );
  }
}

@Injectable({
  providedIn: 'root'
})
export class EditorGuard implements CanActivate {

  constructor(
    private userStore: Store<UserState>,
    private router: Router) {}

  canActivate(): Observable<boolean> {
    return this.userStore
      .select(getUser)
      .pipe(
        map(u => {
          if (u && ['editor', 'admin'].includes(u.role)) { return true; } else {
            this.router.navigate(['/']);
            return false;
          }
        }),
        take(1)
      );
  }
}

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  constructor(
    private userStore: Store<UserState>,
    private router: Router) {}

  canActivate(): Observable<boolean> {
    return this.userStore
      .select(getUser)
      .pipe(
        map(u => {
          if (u && u.role === 'admin') { return true; } else {
            this.router.navigate(['/']);
            return false;
          }
        }),
        take(1)
      );
  }
}
