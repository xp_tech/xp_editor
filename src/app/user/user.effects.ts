import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';

import {UserActionTypes,
        SignIn,
        SignUp,
        Recover,
        NewPassword,
        SignInFailure,
        SignInSuccess,
        SignOutFailure,
        SignOutSuccess,
        SignUpFailure,
        SignUpSuccess,
        RecoverFailure,
        RecoverSuccess,
        NewPasswordFailure,
        NewPasswordSuccess} from './user.actions';
import {switchMap, map, catchError} from 'rxjs/operators';
import {UserService} from './user.service';
import {User} from './user.model';



@Injectable()
export class UserEffects {


  constructor(private actions$: Actions,
              private userService: UserService,
  ) {
  }

  @Effect()
  loadUser: Observable<Action> = this.actions$
    .pipe(
      ofType(UserActionTypes.LoadUser),
      switchMap(() => this.userService.loadUser().pipe(
        map((u: User) => (u) ? new SignInSuccess(u) : new SignOutSuccess()),
        catchError((error): Observable<Action> => (error.status === 401) ? of(new SignOutSuccess()) : of(new SignInFailure(error))
        )
      )),
    );

  @Effect()
  signup: Observable<Action> = this.actions$
    .pipe(
      ofType(UserActionTypes.SignUp),
      switchMap((data: SignUp) => this.userService.signup(data.payload).pipe(
        map((email) => new SignUpSuccess()),
        catchError(error => of(new SignUpFailure(error)))
      )),
    );

  @Effect()
  signin: Observable<Action> = this.actions$
    .pipe(
      ofType(UserActionTypes.SignIn),
      switchMap((data: SignIn) => this.userService.signin(data.payload).pipe(
        map((user) => new SignInSuccess(user)),
        catchError(error => of(new SignInFailure(error)))
      )),

    );

  @Effect()
  recover: Observable<Action> = this.actions$
    .pipe(
      ofType(UserActionTypes.Recover),
      switchMap((data: Recover) => this.userService.recover(data.payload).pipe(
        map((user) => new RecoverSuccess()),
        catchError(error => of(new RecoverFailure(error)))
      )),
    );

  @Effect()
  newPassword: Observable<Action> = this.actions$
    .pipe(
      ofType(UserActionTypes.NewPassword),
      switchMap((data: NewPassword) => this.userService.setNewPassword(data.payload).pipe(
        map((user) => new NewPasswordSuccess()),
        catchError(error => of(new NewPasswordFailure(error)))
      )),
    );

  @Effect()
  signout: Observable<Action> = this.actions$
    .pipe(
      ofType(UserActionTypes.SignOut),
      switchMap(() => this.userService.signout().pipe(
        map(() => {
          return new SignOutSuccess();
        }),
        catchError(error => of(new SignOutFailure(error)))
      )),
    );
}
