import { Action } from '@ngrx/store';
import { User } from './user.model';

export enum UserActionTypes {
  LoadUser = '[User] Load User',
  SignIn = '[User] Sign in',
  SignOut = '[User] Sign out',
  SignUp = '[User] Sign up',
  Recover = '[User] Recover password',
  NewPassword = '[User] Set new password',
  SignInFailure = '[User] Sign in failure',
  SignInSuccess = '[User] Sign in success',
  SignOutFailure = '[User] Sign out failure',
  SignOutSuccess = '[User] Sign out success',
  SignUpFailure = '[User] Sign out failure',
  SignUpSuccess = '[User] Sign out success',
  RecoverFailure = '[User] Recover password failure',
  RecoverSuccess = '[User] Recover password success',
  NewPasswordFailure = '[User] Set new password failure',
  NewPasswordSuccess = '[User] Set new password success',
}

export class LoadUser implements Action {
  readonly type = UserActionTypes.LoadUser;
}


export class SignIn implements Action {
  readonly type = UserActionTypes.SignIn;
  constructor(public payload: {email: string, password: string}) {}
}

export class SignOut implements Action {
  readonly type = UserActionTypes.SignOut;
}

export class SignUp implements Action {
  readonly type = UserActionTypes.SignUp;
  constructor(public payload: {email: string, password: string}) {}
}

export class Recover implements Action {
  readonly type = UserActionTypes.Recover;
  constructor(public payload: {email: string}) {}
}

export class NewPassword implements Action {
  readonly type = UserActionTypes.NewPassword;
  constructor(public payload: {email: string, password: string, code: string}) {}
}

export class SignInFailure implements Action {
  readonly type = UserActionTypes.SignInFailure;
  constructor(public payload: any) {}
}

export class SignInSuccess implements Action {
  readonly type = UserActionTypes.SignInSuccess;
  constructor(public payload: User) {}
}

export class SignOutFailure implements Action {
  readonly type = UserActionTypes.SignOutFailure;
  constructor(public payload: any) {}
}

export class SignOutSuccess implements Action {
  readonly type = UserActionTypes.SignOutSuccess;
}

export class SignUpFailure implements Action {
  readonly type = UserActionTypes.SignUpFailure;
  constructor(public payload: any) {}
}

export class SignUpSuccess implements Action {
  readonly type = UserActionTypes.SignUpSuccess;
}

export class RecoverFailure implements Action {
  readonly type = UserActionTypes.RecoverFailure;
  constructor(public payload: any) {}
}

export class RecoverSuccess implements Action {
  readonly type = UserActionTypes.RecoverSuccess;
}

export class NewPasswordFailure implements Action {
  readonly type = UserActionTypes.NewPasswordFailure;
  constructor(public payload: any) {}
}

export class NewPasswordSuccess implements Action {
  readonly type = UserActionTypes.NewPasswordSuccess;
}



export type UserActions =
  | LoadUser
  | SignIn
  | SignOut
  | SignUp
  | Recover
  | NewPassword
  | SignInFailure
  | SignInSuccess
  | SignOutFailure
  | SignOutSuccess
  | SignUpFailure
  | SignUpSuccess
  | RecoverFailure
  | RecoverSuccess
  | NewPasswordFailure
  | NewPasswordSuccess;
